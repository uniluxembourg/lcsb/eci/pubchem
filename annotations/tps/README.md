# PubChem Transformations

This subfolder is a home for all things related to 
Transformations and Transformation Products (TPs)

## Scripts

The script [extractAnnotations.R](annotations/tps/extractAnnotations.R) is a key cornerstone script
for many ECI efforts. Many workflows depend on it!

The script [Transformations.R](annotations/tps/Transformations.R) is used to create the
Transformation files and create outputs for TP workflows (see next section).
 
Please do not adjust these scripts in master 
(unless specifically cleared). 

## Transformations Summary Files and Folder

A summary of all transformations (merged NORMAN-SLE and ChEMBL) in PubChem is in 
[PubChem_all_transformations.csv](annotations/tps/PubChem_all_transformations.csv).

The same summary of all transformations in PubChem with additional information is in 
[PubChem_all_transformations_wExtraInfo.csv](annotations/tps/PubChem_all_transformations_wExtraInfo.csv).

The `ExtraInfo` file contains, in addition to the merged Transformations information, 
also SMILES, InChIs, InChIKeys, reaction SMILES, 
exact massses and mass differences, XlogP and other reaction identifiers.
Two form of reaction SMILES are available, a plain form (`predecessorSMILES>>successorSMILES`) 
and a more descriptive form including reaction description and CIDs as title. 
These can be plotted _as is_ in [CDK Depict](https://www.simolecule.com/cdkdepict/depict.html). 
Note that due to the presence of reference information, this contains several 
duplicate entries. The `CID_to_CID`, `IK_to_IK` and/or `IKFB_to_IKFB` columns can 
be used to deduplicate as desired. These use the double `>>` to join 
predecessor and successor identifiers analagous to reaction SMILES.

Legacy output (via Jeff) is [norman_transformations_tsv.txt](annotations/tps/norman_transformations_tsv.txt).

These efforts are currently used to contribute data to [patRoon](https://rickhelmus.github.io/patRoon/) 
and [BioTransformer](http://biotransformer.ca/) among others. 

Subfolder [Transformations](annotations/tps/Transformations/) contains several supporting files
for merging and for additional transformation-related data analysis. 

## Dataset Subfolders

Individual datasets each have their own subfolder (except ChEMBL, which is external and 
included in the summaries only). These are primarily for data upload _to_ PubChem within 
the scope of the [NORMAN-SLE](https://www.norman-network.com/nds/SLE/).

A summary of the NORMAN-SLE datasets is given in 
[Transformation_Datasets.txt](annotations/tps/Transformation_Datasets.txt)

| Dataset | DatasetCode | Source |
| ------- | ----------- | ------ |
| S60     | SWISSPEST19 | SLE    |
| S66     | EAWAGTPS    | SLE    |
| S68     | HSDBTPS     | SLE    |
| S73     | METXBIODB   | SLE    |
| S74     | REFTPS      | SLE    |
| S78     | SLUPESTTPS  | SLE    |
| S79     | UACCSCEC    | SLE    |
| ChEMBL  | -External-  | PubChem|


### Archive Subfolder 

Subfolder "archive" contains out-of-date files, for the record.
This includes some of the legacy content. For the record, the legacy README 
note is: 
- CID__norman_s60.csv was generated on 20200918 via getPcCand.trans(' '). Seems to contain n=3312 rows, the merged S60+66+68 underlying PubChem's Transformations section as of 20200918.
