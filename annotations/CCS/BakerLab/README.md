# CCS and Structures from the Baker Lab

This folder contains Collision Cross Section (CCS) data 
and chemical structures provided by Erin Baker from the 
[Baker Lab](https://tarheels.live/bakerlab/) for 
integration within 
[PubChem](https://pubchem.ncbi.nlm.nih.gov/source/25763).

The [FAIR Chemical Structures](https://doi.org/10.1186/s13321-021-00520-4)
and draft [CCS](https://ftp.ncbi.nlm.nih.gov/pubchem/Other/Submissions/)
templates were used to prepare this data for upload during 
[BioHackathon EU 2022](https://biohackathon-europe.org/). 

The Baker Lab have released their data under a CC-BY 4.0 license. 
Please cite their works when using this data!

- Lipids: Kirkwood *et al.* (2022) *J. Proteome Res.* 21, 1, 232–242. DOI:[10.1021/acs.jproteome.1c00820](https://doi.org/10.1021/acs.jproteome.1c00820)
- PFAS: Foster *et al.* (2022) *Environ. Sci. Technol.* 56, 12, 9133–9143. DOI:[10.1021/acs.est.2c00201](https://doi.org/10.1021/acs.est.2c00201)
- More PFAS: Joseph *et al.* (2025). *Scientific Data* 12, 1, 150. DOI:[10.1038/s41597-024-04363-0](https://doi.org/10.1038/s41597-024-04363-0) 
- TOXCAST: Teri *et al.* (in prep.).
- Pesticides and Antidepressants: stay tuned for a publication!

Thanks to the Baker Lab for this contribution!
