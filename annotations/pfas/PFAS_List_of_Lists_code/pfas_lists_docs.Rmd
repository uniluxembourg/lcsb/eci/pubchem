---
title: "Retrieving PFAS Lists on PubChem with PUG REST"
#author: Emma L. Schymanski<sup>1</sup>, Paul A. Thiessen<sup>2</sup>, Jeff Zhang<sup>2</sup> and Evan E. Bolton<sup>2</sup>
author: Emma L. Schymanski^1^, Paul A. Thiessen^2^, Jeff Zhang^2^ and Evan E. Bolton^2^
date: "13/03/2022 (updated 11/12/2024)"
output:
#  word_document: default
  pdf_document: default
#  html_document: default
csl: journal-of-cheminformatics.csl
bibliography: pfas_lists.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

^1^ Luxembourg Centre for Systems Biomedicine (LCSB), 
University of Luxembourg, 6 avenue du Swing, 4367, Belvaux, Luxembourg. 
ELS: ORCID [0000-0001-6868-8145](http://orcid.org/0000-0001-6868-8145)

^2^ National Center for Biotechnology Information, National 
Library of Medicine, National Institutes of Health, Bethesda, MD, 20894, USA. 
PAT: ORCID [0000-0002-1992-2086](http://orcid.org/0000-0002-1992-2086), 
JZ: ORCID [0000-0002-6192-4632](http://orcid.org/0000-0002-6192-4632), 
EEB: ORCID [0000-0002-5959-6190](http://orcid.org/0000-0002-5959-6190)

## Background

This document is based off code developed at 
[BioHackEU2020](https://2020.biohackathon-europe.org/)
arising from PubChemLite efforts [@schymanski_empowering_2021].
The original efforts have a dedicated 
[keyword folder](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/tree/master/annotations/keywords) 
and more extensive documentation within the public ECI GitLab 
[pubchem](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/) repository.
PubChem [@kim_pubchem_2021] Classification Trees are 
[here](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=1) and 
accessed by their respective `hid` number (see Figure 1). 

![_The NORMAN Suspect List Exchange PubChem Classification Browser._](fig\PubChem_Classification_NORMAN_SLE_2024.png)


A set of base functions were developed (ELS, PAT, EEB) based on largely 
undocumented features of the classification trees. These are included in the 
[hid_tree_JSON.R](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/blob/master/annotations/keywords/hids/hid_tree_JSON.R)
and
[hid_tree_functions.R](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/blob/master/annotations/keywords/hids/hid_tree_functions.R)
scripts. The latter is the home for all eventual functions (including those
developed during BioHackathon and beyond), with a 
[contents](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/blob/master/annotations/keywords/hids/hid_tree_functions.R#L6)
listing. The functions are documented using comments above the 
respective function, but not yet in `roxygen`.

To start, set up all packages, download latest scripts and source them: 
```{r initialize} 
library(rjson)
library(httr)
hid_fn_file_url <- "https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/raw/master/annotations/keywords/hids/hid_tree_functions.R"
download.file(hid_fn_file_url,"hid_tree_functions.R")
source("hid_tree_functions.R")
```


### Retrieving one tree file
First, try to retrieve the CompTox tree, hid=105:

```{r get CompTox tree}
tree_csv <- getPcHidTree(105,3)
tree_csv
```

Then, load & take a look:
```{r read CompTox tree}
CompTox_tree <- read.csv(tree_csv, stringsAsFactors = F)

```

For this application, we are most interested in the PFAS entries. 
```{r find PFAS}
i_pfas_lists <- grep("PFAS",CompTox_tree$nodeNames)
CompTox_pfas_lists <- CompTox_tree[i_pfas_lists,]
```

Once we have this output, including node HNIDs, we can build the URL 
needed to retrieve the CID listings per node entry via 
[PUG REST](https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest$classification_nodes).

```{r add URL}
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/classification/hnid/<integer>/<id type>/<format>
hnid_base_url <- "https://pubchem.ncbi.nlm.nih.gov/rest/pug/classification/hnid/"
hnid_end_url <- "/cids/TXT"
CompTox_pfas_lists$REST_URL <- ""

for (i in 1:length(CompTox_pfas_lists$nodeHNID)) {
  CompTox_pfas_lists$REST_URL[i] <- paste0(hnid_base_url,CompTox_pfas_lists$nodeHNID[i],hnid_end_url)
}
```

Note that since the CompTox tree has a nested format, there is a lot of duplication.
Thus, the following trims two columns and uniquifies over the remaining entries:

```{r trim list of lists}
CompTox_pfas_lists <- CompTox_pfas_lists[,c(-8,-9)]
CompTox_pfas_lists <- unique(CompTox_pfas_lists)
```

Now, write output and use this to update the _PFAS List of Lists_:

```{r output list} 
write.csv(CompTox_pfas_lists,file="CompTox_PFAS_lists_in_PubChem.csv",row.names = F)
```

<!-- To be continued ...  -->

<!-- ## R Markdown -->

<!-- This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>. -->

<!-- When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this: -->

<!-- ```{r cars} -->
<!-- summary(cars) -->
<!-- ``` -->

<!-- ## Including Plots -->

<!-- You can also embed plots, for example: -->

<!-- ```{r pressure, echo=FALSE} -->
<!-- plot(pressure) -->
<!-- ``` -->

<!-- Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot. -->

<!-- ### Extra tips: -->

<!-- Make a code chunk with three back ticks followed by an r in braces.  -->
<!-- End the chunk with three back ticks:  -->
<!-- ```{r}  -->
<!-- paste("Hello", "World!")  -->
<!-- ``` -->

<!-- Place code inline with a single back ticks.  -->
<!-- The first back tick must be followed by an R,  -->
<!-- like this `r paste("Hello", "World!")`. -->

<!-- Add chunk options within braces. For example, `echo=FALSE` will prevent  -->
<!-- source code from being displayed:  -->
<!-- ```{r eval=TRUE, echo=FALSE}  -->
<!-- paste("Hello", "World!") -->
<!-- ``` -->

<!-- `include=FALSE` will run the chunk but not include it in the document -->


## References
