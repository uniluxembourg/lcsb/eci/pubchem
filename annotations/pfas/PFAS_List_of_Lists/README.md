# PFAS "List of List" Main Details

- NORMAN-SLE: downloaded 21/11/2021
- CompTox: downloaded 22/22/2021 (pre-20/22/2021 release versions)
- Others: downloaded 21/11/2021

## Extra notes:

### S9_PFASTRIER: 
Removed 16 non-F-containing CIDs (solvents) that need to be removed from the list. 

### S25_OECDPFAS:
Downloaded from the main tree (not annotation part) has this had slightly more CIDs (3692 vs 3677)

### ChEBI
Decided to remove ChEBI: Fluorine Molecular Entity as this also contains inorganics. Also note that the ChEBI node_id numbers are not stable.

### MeSH
Nodes appear stable so far. 
