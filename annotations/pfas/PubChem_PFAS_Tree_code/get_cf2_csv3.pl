#!/usr/bin/perl

use strict;
use warnings;


my %em = ( ( "C", 12.0000000 ), ( "F", 18.9984032 ), ( "N", 14.003074 ),
           ( "O", 15.994914619 ), ( "Si", 27.976926535 ), ( "P", 30.973761998 ),
           ( "S", 31.97207117 ), ( "B", 11.0093051 ), ( "I", 126.9044 ),
           ( "Hg", 201.97064 ), ( "Br", 78.91833 ), ( "Te", 129.9062227 ),
           ( "Se", 79.91652 ), ( "As", 74.92159 ), ( "V", 50.94395 ),
           ( "Bi", 208.98040 ), ( "Fe", 55.93493 ), ( "Ni", 57.93534 ),
           ( "W", 183.95093 ), ( "Re", 186.95575 ), ( "Ir", 192.96292 ),
           ( "Ru", 101.90434 ), ( "Ta", 180.9480 ), ( "Os", 191.96148 ),
           ( "Cl", 34.968852 ), ( "Sb", 120.9038 ), ( "Zr", 89.904698 ),
           ( "Ga", 68.92557 ), ( "Co", 58.93319 ), ( "Nb", 92.90637 ),
           ( "Pu", 244.06420 ), ( "H", 1.007825032 ), ( "Cr", 51.94050 ),
           ( "Cu", 62.92959 ), ( "Ti", 47.947940 ), ( "Pd", 105.90348 ),
           ( "Mn", 54.93804 ), ( "U", 238.05079 ), ( "Np", 237.04817 ),
           ( "Zn", 63.92914 ), ( "Rh", 102.90549 ), ( "Au", 196.96657 ),
           ( "Pt", 194.96479 ), ( "Kr", 83.9114977 ), ( "Xe", 131.9041550 ),
           ( "Am", 243.06138 ), ( "Sr", 87.9056122 ), ( "At", 209.98714 ),
           ( "Ar", 39.9623831 ), ( "Tc", 96.90636 ), ( "Pb", 207.97665 ),
           ( "Y", 88.90584 ), ( "Cd", 113.90336 ), ( "Ce", 139.90545 ),
           ( "Eu", 152.92123 ), ( "In", 114.9038787 ), ( "Sm", 151.91973 ),
           ( "Po", 208.98243 ), ( "Hf", 179.94656 ), ( "Mo", 97.905404 ),
           ( "Pr", 140.90766 ), ( "Tl", 204.97442 ), ( "Pm", 144.91275 ),
           ( "Ag", 106.90509 ), ( "Tb", 158.92535 ), ( "Dy", 163.92918 ),
           ( "La", 138.90636 ), ( "Nd", 141.90773 ), ( "Sc", 44.95590 ),
           ( "Th", 232.03805 ), ( "Ho", 164.93032 ), ( "Tm", 168.93421 ),
           ( "Yb", 173.9388675 ), ( "Lu", 174.94077 ), ( "Er", 165.93029 ),
           ( "Gd", 157.92411 ),
           ( "Al", 26.981538 ), ( "Sn", 119.90220 ), ( "Ge", 73.9211777 ) );
my %or = ( ( "H", undef ), ( "C", undef ), ( "N", undef ), ( "O", undef ),
           ( "F", undef ), ( "Si", undef ), ( "P", undef ), ( "S", undef ),
           ( "Cl", undef ), ( "Br", undef ), ( "I", undef ) );

# Use if PFAS does not contain ether
my %lut = (
  ( "PFAS-C(=O)-OH", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkyl carboxylic acid (PFCA)" ),
  ( "(PFAS-C(=O)-OH)2", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkyl dicarboxylic acid (PFdiCA)" ),
  ( "PFAS-S(=O)2-OH", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfonic acid (PFSA)" ),
  ( "(PFAS-S(=O)2-OH)2", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkane disulfonic acid (PFdiSA)" ),
  ( "PFAS-P(=O)(OH)2", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphonic acid (PFPA)" ),
  ( "PFAS-P(=O)(OH)-PFAS", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphinic acid (PFPIA)" ),
  ( "PFAS-S(=O)-OH", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfinic acid (PFSIA)" ),
  ( "PFAS-CH2-OH", "PFAA\tn:1 Fluorotelomer alcohol" ),
  ( "PFAS-C(=O)-F", "PFAA\tPerfluoroalkanoyl fluoride (PACF)" ),
  ( "PFAS-S(=O)2-F", "PFAA\tPerfluoroalkane sulfonyl fluoride (PASF)" ),
  ( "PFAS-C(=O)-PFAS", "PFAA precursor\tPerfluoroalkyl ketone" ),
  ( "PFAS-OH", "PFAA precursor\tPerfluoroalkyl alcohol" ),
  );
# Use if PFAS contains ether
my %eut = (
  ( "PFAS-C(=O)-OH", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkylether carboxylic acid (PFECA)" ),
  ( "PFAS-S(=O)2-OH", "Perfluoroalkyl acid (PFAA)\tPerfluoroalkylether sulfonic acid (PFESA)" ),
  );
# Use for whole molecule MF cases
my %wut = (
  );
# Use for partial match
my %cmt = (
  ( "PFAS-C(=O)-", "PFAA precursor\tPerfluoroalkanoyl fluorides (PACF)\tPACF-based chemical" ),

  ( "PFAS-S(=O)2-OR", "PFAA precursor\tContains PFSA-based chemical" ),
  ( "PFAS-S(=O)2-AROMATIC", "PFAA precursor\tContains PFSA-based chemical" ),
  ( "PFAS-S(=O)2-NHR", "PFAA precursor\tContains PFSA-based chemical" ),
  ( "PFAS-S(=O)2-NR2", "PFAA precursor\tContains PFSA-based chemical" ),
  ( "PFAS-C(=O)-OR", "PFAA precursor\tContains PFCA-based chemical" ),
  ( "PFAS-C(=O)-NHR", "PFAA precursor\tContains PACF-based chemical" ),
  ( "PFAS-C(=O)-NR2", "PFAA precursor\tContains PACF-based chemical" ),
  ( "PFAS-CH(CH3)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-CH3", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CHR2)(NHR)-OH", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CR2)(OH)-OR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CR2)(OR)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-SR3", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH3)2-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)2-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)2-OR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)(CH3)2", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)(OR)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH3)-NHR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-CHR2", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)2-OH", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-NR2", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(NR2)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)3", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-C(CH2R)(OH)-OR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-OR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-OH", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH(NHR)-AROMATIC", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-OR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-CR2", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-NR2", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-NHR", "PFAA precursor\tContains n:1 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-CHR2", "PFAA precursor\tContains n:2 fluorotelomer-based chemical" ),
  ( "PFAS-CH2-CH2R", "PFAA precursor\tContains n:2 fluorotelomer-based chemical" ),
  ( "PFAS-CH(CH2R)2", "PFAA precursor\tContains n:2 fluorotelomer-based chemical" ),
  ( "PFAS-O-CH2R", "PFAA precursor\tContains hydrofluoroether-based chemical" ),
  ( "PFAS-O-CHR2", "PFAA precursor\tContains hydrofluoroether-based chemical" ),
  ( "PFAS-C(=O)-AROMATIC", "PFAA precursor\tContains semi-fluorinated ketone chemical" ),
  ( "PFAS-S-AROMATIC", "Other PFAS\tContains side-chain fluorinated aromatic chemical" ),
  ( "PFAS-O-AROMATIC", "Other PFAS\tContains side-chain fluorinated aromatic chemical" ),
  ( "PFAS-C(AROMATIC)2", "Other PFAS\tContains side-chain fluorinated aromatic chemical" ),
  ( "PFAS-S-CH2", "Other PFAS\tOther\tContains side-chain thio-ether chemical" ),
          );


my $ifile = shift( @ARGV );
if ( ! defined( $ifile ) || $ifile eq "" ) {
  die "Please provide an input file to process\n";
}

my $ofile = shift( @ARGV );
if ( ! defined( $ofile ) || $ofile eq "" ) {
  die "Please provide an output file to process\n";
}


open( SDF, "/usr/bin/gunzip < $ifile |" ) || die "Unable to read file: $ifile\n";
print STDERR "Reading file: $ifile\n";

my $ofile1 = $ofile . "1";
my $ofile2 = $ofile . "2";
my $ofile3 = $ofile . "3";
my $ofile4 = $ofile . "4";
open( TS1, "> $ofile1" ) || die "Unable to write file: $ofile1\n";
print STDERR "Writing file: $ofile1\n";
open( TS2, "> $ofile2" ) || die "Unable to write file: $ofile2\n";
print STDERR "Writing file: $ofile2\n";
open( TS3, "> $ofile3" ) || die "Unable to write file: $ofile3\n";
print STDERR "Writing file: $ofile3\n";
open( TS4, "> $ofile4" ) || die "Unable to write file: $ofile4\n";
print STDERR "Writing file: $ofile4\n";

#print TS1 "CID\tMF\tExactMass\t#AromaticAtom\t#NonaromaticAtom\t#FluorinatedAliphaticAtom",
#      "\t#FluorinatedUnsaturatedAtom\t#FluorinatedAromaticAtom",
#      "\t#OECDpfasCarbonAtom\t#FluorineAtom";
#print TS2 "\tCID\tPFASFragmentMF\tType\tFragmentType\tUnsaturation\t#Atoms\t#Bonds",
#      "\t#PFASAtoms\t#FluorineAtoms\tFragmentMass\tFlags\tOECDAtom\tNonOECDAtoms",
#      "\n";


my $nline = 0;
my @l = ();
my $n = 0;
while ( $_ = <SDF> ) {
  $nline++;
  chop( $_ );
  $l[ $n++ ] = $_;

  if ( $_ ne "\$\$\$\$" ) {
    next;  # Read entire record and then process it
  }

  if ( $n < 4 ) {
    die "Corrupt SDF record priot to line: $nline .. fewer than 4 lines!\n", join( "\n", @l ), "\n";
  }


  # Complete SDF record
  #
  # Read the counts line
  # .. loop over atoms and bonds
  # .. remove unwanted bonds
  # .. remove atoms without bonds
  # .. dump updated SDF record

  # Get atom/bond count
  my $na = substr( $l[3], 0, 3 );
  my $nb = substr( $l[3], 3, 3 );


  my $m = 4 + $na;
  my $o = $m + $nb;
  if ( $n < $o ) {
    die "Corrupt SDF record .. fewer than $o lines!\n", join( "\n", @l ), "\n";
  }


  # Loop over atoms and find fluorine atoms "F "
  my $has_f = 0;
  for ( my $i = 4;  $i < $m;  $i++ ) {
    my $e = substr( $l[$i], 31, 2 );
    if ( $e eq "F " ) {
      $has_f++;
      last;  # Need a Fluorine atom to continue
    }
  }


  # Is structure in scope?
  if ( $has_f == 0 ) {  # Skip cases of non-fluorine containing structures
#    print STDERR ":: Not a PFAS ::\n\n\n";
    $n = 0;
    @l = ();
    next;  # Work on the next SDF record
  }


  # Get the CID and anything else of interest
  my %aa = ();  # keep track of aromatic atoms/bonds
  my $cid = "";
  my $smi = "";
  my $mf = "";
  my $ms = "";
  my $ik = "";
  my $do_skip = 0;
  for ( my $i = $o;  $i < $n;  $i++ ) {
    if ( $l[$i] =~ /\> \</ ) {
      $l[$i] =~ /\> \<(.+)\>/;
      my $tag = $1;
      if ( $tag eq "PUBCHEM_COMPOUND_CID" ) {
        $cid = $l[$i+1];
        print STDERR "\n\nCID:$cid\n";
        $i += 2;
      } elsif ( $tag eq "PUBCHEM_BONDANNOTATIONS" ) {
        $i++;
        while ( $i < $n ) {
          if ( $l[ $i ] eq "" ) { last; }

          my @at = split( /\s+/, $l[ $i ], 3 );
          if ( $at[2] == 8 ) {  # Aromatic bond
            $aa{ $at[0] }{ $at[1] } = undef;
            $aa{ $at[1] }{ $at[0] } = undef;
          }

          $i++;
        }

        $i++;
      } elsif ( $tag eq "PUBCHEM_IUPAC_INCHIKEY" ) {
        $ik = $l[$i+1];
        $i += 2;
      } elsif ( $tag eq "PUBCHEM_EXACT_MASS" ) {
        $ms = $l[$i+1];
        $i += 2;
      } elsif ( $tag eq "PUBCHEM_OPENEYE_ISO_SMILES" ) {
        $smi = $l[$i+1];
        $i += 2;
        # Ignore multicomponent structures
        if ( $smi =~ /\./ ) {
          print STDERR "Ignored multicomponent structure CID $cid :: SMILES: $smi\n";
          $do_skip = 1;  # Skip record but read other key info
        }
      } elsif ( $tag eq "PUBCHEM_MOLECULAR_FORMULA" ) {
        $mf = $l[$i+1];
        $i += 2;
      } else {
        $i += 2;
#      } else {
#        print STDERR "Skipped SDTAG: $tag\n";
      }

    # Ignore radicals and isotopically labelled structures
    } elsif ( $l[$i] =~ /M  ISO|M  RAD/ ) {
      $do_skip = 2;  # Skip record but read other key info
    }
  }


  # Is structure in scope?  (Removes multicomponent and isotopically labelled and radical compounds)
  if ( $do_skip != 0 ) {
    if ( $do_skip == 2 ) { print STDERR "Ignored structure w/ isotope or radical CID $cid :: SMILES: $smi\n"; }
    elsif ( $do_skip == 1 ) { print STDERR "Ignored multicomponent structure  CID $cid :: SMILES: $smi\n"; }

    $n = 0;
    @l = ();
    next;  # Work on the next SDF record
  }


  print STDERR "$nline :: Read Record CID:$cid ::\n";


  my $naa = keys( %aa );
  print STDERR "$nline :: Atom, Bond, and Aromatic Atom COUNTS :: $na $nb $naa .. $l[3]\n";


  my %e = ();  # Element counts
  my %a = ();  # Atom IDs that are not H, F, Cl, Br, or I
  my %af = ();  # Atom IDs that are Fluorine
  my %ah = ();  # Atom IDs that are Hydrogen
  my %ax = ();  # Atom IDs that are Cl, Br, or I
  for ( my $i = 4;  $i < $m;  $i++ ) {
    my $s = substr( $l[$i], 31, 2 );
    my $ai = sprintf( "%d", $i - 3 );

#print STDERR "element before: \"$s\"\n";
    if ( substr( $s, 1, 1 ) eq " " ) {
      chop( $s );  # Remove space
    }
#print STDERR "element after: \"$s\"\n";


    # Element histogram
    if ( exists( $e{ $s } ) ) {
      $e{ $s }++;
    } else {
      $e{ $s } = 1;
    }


    # Track atom IDs
    if ( $s eq "H" ) {
      $ah{ $ai } = undef;  # Track hydrogen
    } elsif ( $s eq "F" ) {
      $af{ $ai } = undef;
      print STDERR "F found! :: $ai .. $na $nb\n";
    } elsif ( $s eq "Cl" || $s eq "Br" || $s eq "I" ) {
      $ax{ $ai } = undef;  # Track other halogen
    } elsif ( $s eq "D" || $s eq "T" ) {
      print STDERR "Ignored structure w/ isotope CID $cid :: SMILES: $smi\n";
      $do_skip = 1;
      last;
    } else {  # Allow all other atoms
      $a{ $ai } = 0;  # Atom IDs for atoms that are not H, F, Cl, Br, or I
    }
  }


  # Is this structure in scope?  (Removes isotopically labelled hydrogen)
  if ( $do_skip == 1 ) {
    $n = 0;
    @l = ();
    next;  # Work on the next SDF record
  }


  # Determine count of elements
  my $nf = keys( %af );  # Fluorine count
  my $nx = keys( %ax );  # Other halogen count
  my $nha = keys( %a ) + $nf + $nx;  # Heavy atom count

  my $ninorg = 0;
  foreach my $s ( keys( %e ) ) {
    if ( ! exists( $or{ $s } ) ) {
      $ninorg += $e{ $s };
    }
  }

  print STDERR "$nline :: Fluorine: $nf  Other Halogen: $nx  Heavy Atoms: $nha  Inorganic Atoms: $ninorg\n";



  # Loop over bonds and create atom connectivity map
  my $nncf = 0;
  my %eb = ();  # Element bonds (which elements connected to which elements w/ their count)
  my %be = ();  # Element bonds (which elements connected to which elements w/ their count)
  my %b = ();  # Bonds to non-hydrogen atoms
  my %acf = ();  # Atom IDs that are connected to Fluorine
  my %ach = ();  # Atom IDs that are connected to Hydrogen
  my %acx = ();  # Atom IDs that are connected to Cl, Br, I
  my %ua = ();  # Unsaturated bonds (but not aromatic)
  for ( my $i = $m;  $i < $o;  $i++ ) {
    # Bond IDs and bond order
    my $b1 = sprintf( "%d", substr( $l[$i], 0, 3 ) );
    my $b2 = sprintf( "%d", substr( $l[$i], 3, 3 ) );
    my $bo = sprintf( "%d", substr( $l[$i], 6, 3 ) );


    # Element symbols
    my $s1 = substr( $l[$b1+3], 31, 2 );
    if ( substr( $s1, 1, 1 ) eq " " ) { chop( $s1 ); }  # Remove trailing space
    my $s2 = substr( $l[$b2+3], 31, 2 );
    if ( substr( $s2, 1, 1 ) eq " " ) { chop( $s2 ); }  # Remove trailing space

    if ( ! ( $bo == 1 || $bo == 2 || $bo == 3 ) ) {
      print STDERR ":: ERROR :: Unrecognized bond order \"$bo\" for CID $cid :: $l[$i]\n";
    }


    # Determine particular bonding situations
    if ( exists( $aa{ $b1 } ) && exists( $aa{ $b1 }{ $b2 } ) ) {  # Is this an aromatic bond?
      $bo = "1.5";
    } elsif ( $bo != 1 ) {  # Is this an unsaturated bond?
      $ua{ $b1 }{ $b2 } = $bo;
      $ua{ $b2 }{ $b1 } = $bo;
    }


    # Track and count bonds between elements including bond order info
    if ( exists( $eb{ $s1 }{ $s2 }{ $bo } ) ) {
      $eb{ $s1 }{ $s2 }{ $bo }++;
    } else {
      $eb{ $s1 }{ $s2 }{ $bo } = 1;
    }

    if ( exists( $eb{ $s2 }{ $s1 }{ $bo } ) ) {
      $eb{ $s2 }{ $s1 }{ $bo }++;
    } else {
      $eb{ $s2 }{ $s1 }{ $bo } = 1;
    }

    $be{ $b1 }{ $b2 } = $bo;
    $be{ $b2 }{ $b1 } = $bo;


    my $db = 1;  # Track bond?


    # Process bond information
    if ( exists( $ah{ $b1 } ) ) {  # Atom is hydrogen
      if ( defined( $ah{ $b1 } ) ) {
        print STDERR ":: ERROR :: Strange bonding situation for hydrogen connected to more than one atom for CID $cid\n";
        $do_skip = 1;
        last;
      }

      $ah{ $b1 } = $b2;

      # Track hydrogen count
      if ( exists( $ach{ $b2 } ) ) {
        $ach{ $b2 }++;
      } else {
        $ach{ $b2 } = 1;
      }

      $db = 0;  # Ignore bond
    } elsif ( exists( $ax{ $b1 } ) ) {  # Atom is Cl/Br/I
      if ( defined( $ax{ $b1 } ) ) {
#        print STDERR ":: WARNING :: Potential strange bonding situation for halogen connected to more than one atom for CID $cid\n";
      } else {
        $ax{ $b1 } = $b2;
      }

      # Track Cl/Br/I count
      if ( exists( $acx{ $b2 } ) ) {
        $acx{ $b2 }++;
      } else {
        $acx{ $b2 } = 1;
      }

      $db = 0;  # Ignore bond
    } elsif ( exists( $af{ $b1 } ) ) {  # Atom is fluorine
      if ( defined( $af{ $b1 } ) ) {
        print STDERR ":: ERROR :: Strange bonding situation for fluorine connected to more than one atom for CID $cid\n";
        $do_skip = 1;
        last;
      }

      $af{ $b1 } = $b2;

      # Track fluorine count
      if ( exists( $acf{ $b2 } ) ) {
        $acf{ $b2 }++;
      } else {
        $acf{ $b2 } = 1;
      }
      print STDERR ":: BOND contains F :$b2: $b1 - $b2  $s1-$s2 :$i: $l[$i]\n";

      if ( $s2 ne "C" && exists( $or{ $s2 } ) ) { $nncf++; }

      $db = 0;  # Ignore bond
    }

    if ( exists( $ah{ $b2 } ) ) {  # Atom is hydrogen
      if ( defined( $ah{ $b2 } ) ) {
        print STDERR ":: ERROR :: Strange bonding situation for hydrogen connected to more than one atom for CID $cid\n";
        $do_skip = 1;
        last;
      }

      $ah{ $b2 } = $b1;

      # Track hydrogen count
      if ( exists( $ach{ $b1 } ) ) {
        $ach{ $b1 }++;
      } else {
        $ach{ $b1 } = 1;
      }

      $db = 0;  # Ignore bond
    } elsif ( exists( $ax{ $b2 } ) ) {  # Atom is Cl/Br/I
      if ( defined( $ax{ $b2 } ) ) {
#        print STDERR ":: WARNING :: Potential strange bonding situation for halogen connected to more than one atom for CID $cid\n";
      } else {
        $ax{ $b2 } = $b1;
      }


      # Track Cl/Br/I count
      if ( exists( $acx{ $b1 } ) ) {
        $acx{ $b1 }++;
      } else {
        $acx{ $b1 } = 1;
      }

      $db = 0;  # Ignore bond
    } elsif ( exists( $af{ $b2 } ) ) {  # Atom is fluorine
      if ( defined( $af{ $b2 } ) ) {
        print STDERR ":: ERROR :: Strange bonding situation for fluorine connected to more than one atom for CID $cid\n";
        $do_skip = 1;
        last;
      }

      $af{ $b2 } = $b1;

      # Track fluorine count
      if ( exists( $acf{ $b1 } ) ) {
        $acf{ $b1 }++;
      } else {
        $acf{ $b1 } = 1;
      }
      print STDERR ":: BOND contains F :$b1: $b1 - $b2  $s1-$s2 :$i: $l[$i]\n";

      if ( $s1 ne "C" && exists( $or{ $s1 } ) ) { $nncf++; }

      $db = 0;  # Ignore bond
    }


    # Track bond if not already handled
    if ( $db == 1 ) {
      # add to the bond map only bonds that are not to H or F or {Cl, Br, I}
      $b{ $b1 }{ $b2 } = $bo;
      $b{ $b2 }{ $b1 } = $bo;
    }
  }


  # Is this structure in scope?  (Removes odd bonding situations)
  if ( $do_skip == 1 ) {
    $n = 0;
    @l = ();
    next;  # Work on the next SDF record
  }


  # Classify this organofluorine compound
  my $contains_aromatic_atom = $naa;
  my $contains_nonaromatic_atom = $nha - $naa - $nf - $nx;
  my $contains_fluorinated_aliphatic_atom = 0;
  my $contains_fluorinated_unsaturated_atom = 0;
  my $contains_fluorinated_aromatic_atom = 0;
  my $contains_oecd_pfas_atom = 0;
  my @acf = keys( %acf );
  foreach my $acf ( @acf ) {
    my $s = substr( $l[$acf+3], 31, 2 );
    if ( $s ne "C " ) { next; }  # Atom connected to fluorine must be carbon

    if ( exists( $aa{ $acf } ) ) {
      $contains_fluorinated_aromatic_atom++;
    } else {
      $contains_fluorinated_aliphatic_atom++;

      if ( exists( $ua{ $acf } ) ) {
        $contains_fluorinated_unsaturated_atom++;
      } else {
        # OECD PFAS atom must be carbon w/out hydrogen or halogen attached other than fluorine and cannot be unsaturated
        if ( ! ( exists( $ach{ $acf } ) || exists( $acx{ $acf } ) ) &&
             $acf{ $acf } > 1 ) {
          $contains_oecd_pfas_atom++;
        }
      }
    }
  }

  my $el = "";  # Element list w/ count w/in structure
  foreach my $e ( sort( keys( %e ) ) ) {
    if ( $el ne "" ) { $el .= " "; }
    $el .= $e . ":" . $e{ $e };
  }


  my $bel = "";  # Bond list w/ elements, bond order, and count w/in structure
  foreach my $e1 ( sort( keys( %eb ) ) ) {
    foreach my $e2 ( sort( keys( %{ $eb{ $e1 } } ) ) ) {
      foreach my $bo ( sort( keys( %{ $eb{ $e1 }{ $e2 } } ) ) ) {
        my $ntb = $eb{ $e1 }{ $e2 }{ $bo };
        if ( $bel ne "" ) { $bel .= " "; }
        $bel .= $e1 . ":" . $e2 . ":" . $bo . ":" . $ntb;
      }
    }
  }


  # Write out atom types
  my %aat = ();
  my %at = ();
  foreach my $a1 ( keys( %be ) ) {
    my $at = substr( $l[$a1+3], 31, 2 );
    if ( substr( $at, 1, 1 ) eq " " ) { chop( $at ); }  # Remove trailing space

    if ( exists( $aa{ $a1 } ) ) { $at =~ tr/A-Z/a-z/; }


    my %ta = ();
    foreach my $a2 ( keys( %{ $be{ $a1 } } ) ) {
      my $s2 = substr( $l[$a2+3], 31, 2 );
      if ( substr( $s2, 1, 1 ) eq " " ) { chop( $s2 ); }  # Remove trailing space
      if ( exists( $aa{ $a2 } ) ) { $s2 =~ tr/A-Z/a-z/; }
      
      my $bo = $be{ $a1 }{ $a2 };
      if ( $bo == 1 ) {
        $bo = "";
      } elsif ( $bo == 2 ) {
        $bo = "=";
      } elsif ( $bo == 1.5 ) {
        $bo = ":";
      } elsif ( $bo == 3 ) {
        $bo = "#";
      } else {
        print STDERR ":: ERROR :: Unrecognized bond order \"$bo\" for CID: $cid\n";
      }

      $bo .= $s2;

      if ( exists( $ta{ $bo } ) ) {
        $ta{ $bo }++;
      } else {
        $ta{ $bo } = 1;
      }
    }

    my @ta = sort( keys( %ta ) );
    my $ntta = @ta;
    if ( $ntta == 1 ) {
      my $nta = $ta{ $ta[0] };
      if ( $nta != 1 ) {
        $at .= "(" . $ta[0] . ")" . sprintf( "%d", $nta );
      } else {
        $at .= $ta[0];
      }
    } else {
      $ntta--;
      for ( my $ii = 0;  $ii < $ntta;  $ii++ ) {
        my $nta = $ta{ $ta[$ii] };
        if ( $nta != 1 ) {
          $at .= "(" . $ta[$ii] . ")" . sprintf( "%d", $nta );
        } else {
          $at .= "(" . $ta[$ii] . ")";
        }
      }
      my $nta = $ta{ $ta[$ntta] };
      if ( $nta != 1 ) {
        $at .= "(" . $ta[$ntta] . ")" . sprintf( "%d", $nta );
      } else {
        $at .= $ta[$ntta];
      }
    }

    if ( exists( $at{ $at } ) ) {
      $at{ $at }++;
    } else {
      $at{ $at } = 1;
    }

    $aat{ $a1 } = $at;
  }

  my $sat = "";
  foreach my $ta ( sort( keys( %at ) ) ) {
    my $nta = $at{ $ta };
    if ( $sat ne "" ) { $sat .= " "; }
    $sat .= sprintf( "%d", $nta ) . ":" . $ta;
  }


  print TS1 "$cid",
	  "\t$mf",
	  "\t$ms",
	  "\t$contains_aromatic_atom",
	  "\t$contains_nonaromatic_atom",
	  "\t$contains_fluorinated_aliphatic_atom",
	  "\t$contains_fluorinated_unsaturated_atom",
	  "\t$contains_fluorinated_aromatic_atom",
	  "\t$contains_oecd_pfas_atom",
	  "\t$nf",
	  "\t$ninorg",
	  "\t$nncf",
          "\t$el",
          "\t$bel",
          "\t$sat",
          "\n";



  #
  # PFAS fragments must have one or more atoms connected to two or more fluorine
  #   that are also not attached to H, Cl, Br, or I
  #
  # Loop over atoms attached to fluorine -- refine the PFAS atom list into three groups
  my %cf = ();  # Saturated carbon atom connected to fluorine .. possible OECD PFAS atom
  my %cacf = ();  # Candidate connecting (non)PFAS atoms
  foreach my $acf ( @acf ) {
    my $s = substr( $l[$acf+3], 31, 2 );
    print STDERR "Candidate PFAS Atom: $acf $s\n";

    # Detect Carbon atoms connected to fluorine if they are also attached to hydrogen
    if ( $s ne "C " ) {  # Non-carbon atom
      print STDERR "  Non PFAS atom $acf $s attached to fluorine .. but is not Carbon\n";
    } elsif ( exists( $ach{ $acf } ) ) {  # Carbon also connected to hydrogen
      print STDERR "  Non PFAS Carbon $acf attached to fluorine .. but attached to hydrogen\n";
    } elsif ( exists( $acx{ $acf } ) ) {  # Carbon also connected to other halogen
      print STDERR "  Non PFAS Carbon $acf attached to fluorine .. but attached to Cl, Br, or I\n";
    } elsif ( exists( $aa{ $acf } ) ) {  # Aromatic Carbon attached to fluorine
      print STDERR "  Non PFAS Carbon $acf attached to fluorine .. but is an aromatic atom\n";
    } elsif ( exists( $ua{ $acf } ) ) {  # Unsaturated Carbon attached to fluorine
      print STDERR "  Unsaturated Carbon $acf attached to fluorine .. possible unsaturated OECD PFAS atom\n";
      $cacf{ $acf } = $acf{ $acf };  # Unsaturated Carbon attached to fluorine
    } elsif ( $acf{ $acf } < 2 ) {  # Saturated carbon atom with less than 2 fluorine atoms
      print STDERR "  Carbon Atom $acf attached to fluorine .. possible connecting PFAS atom\n";
      $cacf{ $acf } = $acf{ $acf };  # Carbon attached to one fluorine
    } else {
      print STDERR "  Carbon atom $acf attached to fluorine .. possible OECD PFAS atom\n";
      $cf{ $acf } = $acf{ $acf };  # Carbon attached to fluorine
    }
  }


  # Determine how many atoms connected to fluorine remain
  @acf = keys( %cf );  # PFAS atoms that remain
  my $nacf = @acf;
  if ( $nacf == 0 ) {
    print STDERR ":: Not a PFAS structure CID $cid :: SMILES: $smi\n";
    $n = 0;
    @l = ();
    next; # Need one or more allowed atoms connected to fluorine to be a PFAS
  }

  my $ncacf = keys( %cacf );
  print STDERR "$nline :: CID: $cid .. # PFAS atoms: $nacf  # Potential PFAS atoms: $ncacf\n";


  # Look for candidate atoms that connect PFAS atoms
  #
  # Find all candidate connecting PFAS atoms
  foreach my $a ( keys( %a ) ) {  # Loop over all atoms not H or a halogen
    if ( ! exists( $b{ $a } ) ) { next; }  # Skip weird cases

    # Atom must be C, not bonded to H or X, not aromatic or unsaturated or
    #   already considered (i.e., attached to fluorine)
    my $s = substr( $l[$a+3], 31, 2 );  # Atomic symbol
    if ( $s eq "C " &&
         ! ( exists( $ach{ $a } ) || exists( $acx{ $a } ) || exists( $acf{ $a } ) ||
             exists( $aa{ $a } ) || exists( $ua{ $a } ) || exists( $cacf{ $a } ) ) ) {
      print STDERR "Candidate PFAS connecting atom is $a \"$s\"\n";
      $cacf{ $a } = 0;
    }
  }

  # Look at candidate PFAS atoms .. trim if not connected to a PFAS or candidate PFAS atom
  my %ccacf = ();
  my $nloop = 0;
  my $did_delete = 1;
  while ( $did_delete != 0 ) {  # Loop to trim cases of cacf that are connected to just themselves
    $did_delete = 0;
    my @cacf = keys( %cacf );
    foreach my $a ( @cacf ) {
      if ( ! exists( $b{ $a } ) ) { next; }  # Skip weird cases

      # Loop over connected atoms
      my $nb_cf = 0;

      foreach my $ca ( keys( %{ $b{ $a } } ) ) {  # Loop over atoms connected to atom
        my $s = substr( $l[$a+3], 31, 2 );  # Atomic symbol
        if ( $s ne "C " ) {
          next;
        } elsif ( exists( $cf{ $ca } ) ) {  # connected to a PFAS atom?
          $nb_cf++;
          print STDERR "    $a is attached to PFAS atom $ca .. $aat{$a}\n";
        } elsif ( exists( $cacf{ $ca } ) ) {  # connected to candidate connecting PFAS atom
          $nb_cf++;
          print STDERR "    $a is attached to a candidate connecting PFAS atom $ca .. $aat{$a}\n";
        }
      }

      # All bonds to atoms connected to PFAS or other candidate PFAS atoms?
      my $tnb = keys( %{ $b{ $a } } );
      if ( $nb_cf > 1 &&
           $tnb == $nb_cf ) {
        print STDERR "  Candidate connecting PFAS atom $a ($tnb bonds) is retained: connected to $nb_cf potential PFAS atoms .. $aat{$a}\n";
      } elsif ( exists( $acf{ $a } ) || $nb_cf > 2 ) {  # Consider interesting branched cases w/ fluorine
        if ( exists( $acf{ $a } ) ) { $nb_cf += $acf{ $a }; }  # Add in any attached fluorines
        if ( $nb_cf > 2 ) {
          print STDERR "  Candidate connecting PFAS atom $a ($nb_cf bonds/fluorines) is retained: connected to $nb_cf potential PFAS (or fluorine) atoms .. $aat{$a}\n";
        } else {
          print STDERR "  Candidate connecting PFSA atom $a ($nb_cf bonds/fluorines) is not retained .. $aat{$a}\n";
          $ccacf{ $a } = $cacf{ $a };
          delete( $cacf{ $a } );
          $did_delete++;
        }
      } else {
        print STDERR "  Candidate connecting PFSA atom $a ($nb_cf bonds) is not retained .. $aat{$a}\n";
        $ccacf{ $a } = $cacf{ $a };
        delete( $cacf{ $a } );
        $did_delete++;
      }
    }
  }

  my $ncacf_a = keys( %cacf );
  if ( ( $ncacf_a - $ncacf ) > 3 ) {
    print STDERR ":: WARNING :: Weird case of ", ( $ncacf_a - $ncacf ), " candidate PFAS atoms .. CID: $cid .. SMI: $smi\n";
  }


  #
  # Determine the size of each fragment
  #
  # Loop over allowed atoms attached to fluorine  -- check if they are connected to each other
  my $na_acf = 0;  # Total PFAS atoms in a molecule
  my %ig = ();  # Track cases of AtomIDs already considered in a fragment
  my %frags = ();  # Track FragID plus AtomIDs in the fragment
  my %apfas = ();  # Track what fragment a given AtomID is in
  my $npfas_frag = 0;  # Count of PFAS fragments
  foreach my $acf ( keys( %cf ) ) {
    if ( exists( $ig{ $acf } ) ) { next; }  # PFAS atom was already considered

    my @stack = ();
    push( @stack, $acf );  # seed the process
    $npfas_frag++;

    my $nbond = 0;
    while ( my $ac = shift( @stack ) ) {
      $ig{ $ac } = 0; # Ignore this atom from future consideration
      $frags{ $npfas_frag }{ $ac } = undef;
      $apfas{ $ac } = $npfas_frag;

      if ( exists( $cf{ $ac } ) ) {
        print STDERR "PFAS Frag $npfas_frag: Atom $ac attached to $cf{ $ac } fluorine atoms\n";
      } else {
        print STDERR "PFAS Frag $npfas_frag: Atom $ac attached to $cacf{ $ac } PFAS or fluorine atoms\n";
      }

      if ( ! exists( $b{ $ac } ) ) { next; }  # Skip weird cases

      foreach my $a ( keys( %{ $b{ $ac } } ) ) {  # Loop over connected atoms
        # Attached to atom attached to fluorine and not already considered
        if ( ( exists( $cf{ $a } ) || exists( $cacf{ $a } ) ) && ! exists( $ig{ $a } ) ) {
          push( @stack, $a );  # Consider this PFAS atom
        }
      }
    }

    my @atoms = sort bynum( keys( %{ $frags{ $npfas_frag } } ) );
    my $natoms = @atoms;
    $na_acf += $natoms;
    print STDERR "  PFAS Fragment $npfas_frag contains $natoms PFAS atoms: ",
                 join( " ", @atoms ), "\n";
  }
  print STDERR "There are $na_acf of $na atoms within PFAS fragments\n";
  print STDERR "There are $npfas_frag fragments in total\n";


  # Determine the size of each fragment and compute other aspects of the fragment
  my $nb_acf = 0;  # Total bonds between PFAS atoms in a molecule
  my %bfrags = ();  # Count of PFAS bonds per fragment
  my %cfrags = ();  # Count of connected PFAS atoms per atom
  my %sfrags = ();  # Degree of unsaturation per PFAS fragment
  my %afrags = ();  # Atoms in fragset
  my %cyfrag = ();  # Cycle in fragset per PFAS fragment (if exists, is cycle)

  # Count the bonds in a given fragment and in total
  for ( my $i = $m;  $i < $o;  $i++ ) {
    my $b1 = sprintf( "%d", substr( $l[$i], 0, 3 ) );
    my $b2 = sprintf( "%d", substr( $l[$i], 3, 3 ) );
    my $bo = sprintf( "%d", substr( $l[$i], 6, 3 ) );

    # Are these PFAS atoms in same fragment?
    if ( exists( $apfas{ $b1 } ) &&
         exists( $apfas{ $b2 } ) ) {
      if ( $apfas{ $b1 } != $apfas{ $b2 } ) {
        die "Something is seriously wrong .. connected PFAS atoms $b1-$b2 are not in same fragment",
            " ($apfas{ $b1 } != $apfas{ $b2 } )\n";
      }

      # Track the count of bonds per fragment
      my $frag_id = $apfas{ $b1 };
      $nb_acf++;  # total PFAS bond count
      if ( exists( $bfrags{ $frag_id } ) ) {
        $bfrags{ $frag_id }++;
      } else {
        $bfrags{ $frag_id } = 1;
      }

      # Track count of connected PFAS atoms per atom (to determine if linear or branched)
      if ( exists( $cfrags{ $b1 } ) ) {
        $cfrags{ $b1 }++;
      } else {
        $cfrags{ $b1 } = 1;
      }
      if ( exists( $cfrags{ $b2 } ) ) {
        $cfrags{ $b2 }++;
      } else {
        $cfrags{ $b2 } = 1;
      }

      # Determine if the molecule is unsaturated
      if ( exists( $sfrags{ $frag_id } ) ) {
        $sfrags{ $frag_id } += $bo - 1;
      } else {
        $sfrags{ $frag_id } = $bo - 1;
      }

      # Determine if cyclic
      if ( exists( $afrags{ $frag_id }{ $b1 } ) && exists( $afrags{ $frag_id }{ $b2 } ) ) {
        $cyfrag{ $frag_id } = undef;  # cyclic!
#print STDERR "  Bond: $b1 $b2 in fragment $frag_id .. \n";
      } else {
        $afrags{ $frag_id }{ $b1 } = undef;
        $afrags{ $frag_id }{ $b2 } = undef;
#print STDERR "  Bond: $b1 $b2 in fragment $frag_id\n";
      }
    }
  }
  print STDERR "There are $nb_acf of $nb bonds within PFAS fragments\n";
  foreach my $ifrag ( sort bynum( keys( %bfrags ) ) ) {
    print STDERR "  Fragment $ifrag has $bfrags{ $ifrag } bonds\n";
  }


  # Determine the fragment type "linear/branched/cyclic"
  my %tfrags = ();  # Type of fragment "linear/branched/cyclic"

  # If a single PFAS fragment and atom/bond count is same as whole molecule
  my $ftype_prefix = "";
  if ( $na_acf == $na &&
       $nb_acf == $nb ) {
    $ftype_prefix = "molecule-";
  } else {
    print STDERR "NOT ( $na_acf == $na && $nb_acf == $nb )\n";
  }

  foreach my $ifrag ( sort bynum( keys( %bfrags ) ) ) {
    my @atoms = keys( %{ $frags{ $ifrag } } );
    my $nfrag_c = @atoms;
    my $nbonds = $bfrags{ $ifrag };

    my $frag_type = "linear";
    if ( exists( $cyfrag{ $ifrag } ) ) {
      $frag_type = "cyclic";
    } else {
      foreach my $a ( @atoms ) {
        if ( ! exists( $cfrags{ $a } ) ) {  die "Something is seriously wrong .. atom $a not defined for fragment $ifrag!\n"; }

        if ( $cfrags{ $a } > 2 ) {
          $frag_type = "branched";
          last;
        }
      }
    }

    print STDERR "PFAS Fragment $ifrag is of type: $frag_type  .. with $nfrag_c atoms and $nbonds bonds\n";
    $tfrags{ $ifrag } = $ftype_prefix . $frag_type;
  }


  # Compute counts to report fragments
  my %mffrags = ();  # Molecular formula per frag
  foreach my $ifrag ( sort bynum( keys( %frags ) ) ) {
    my @atoms = keys( %{ $frags{ $ifrag } } );
    my $nfrag_a = @atoms;

    # Count the attached fluorines
    my %fmf = ();
    my $oecd_atom = 0;
    my $not_oecd_atom = 0;
    my $nfrag_f = 0;
    foreach my $a ( @atoms ) {
      if ( exists( $acf{ $a } ) ) {  # Don't assume atom contains fluorine
        $nfrag_f += $acf{ $a };
      }

      # Is this an OECD PFAS Atom?  (attached to 2 or more fluorines, is carbon, not connected to other halogen or hydrogen)
      if ( exists( $acf{ $a } ) && $acf{ $a } > 1 &&
           exists( $cf{ $a } ) &&
           ! ( exists( $ach{ $a } ) || exists( $acx{ $a } ) ) ) {
        $oecd_atom++;
      } else {
        $not_oecd_atom++;
      }

      # Build MF
      my $s = substr( $l[$a+3], 31, 2 );  # Atomic symbol
      if ( substr( $s, 1, 1 ) eq " " ) { chop( $s ); }  # Remove trailing space
      if ( exists( $fmf{ $s } ) ) {
        $fmf{ $s }++;
      } else {
        $fmf{ $s } = 1;
      }
    }
    $fmf{ "F" } = $nfrag_f;

    # Get bond count
    my $nbonds = 0;
    if ( exists( $bfrags{ $ifrag } ) ) {
      $nbonds = $bfrags{ $ifrag };
    }

    # Get degree of unsaturation
    my $nunsat = 0;
    if ( exists( $sfrags{ $ifrag } ) ) {
      $nunsat = $sfrags{ $ifrag };
    }

    # Compute total fragment atom/bond count
    my $nfrag_atoms = $nfrag_a + $nfrag_f;
    my $nfrag_bonds = $nbonds + $nfrag_f;

    # Compute MF and exact mass of fragment
    my $frag_mf = "";
    my $mass = 0.0;
    if ( exists( $fmf{ "C" } ) ) {  # Always put carbon first
      $mass += $fmf{ "C" } * 12.0000000;
      if ( $fmf{ "C" } == 1 ) {
        $frag_mf = "C";
      } else {
        $frag_mf = sprintf( "C%d", $fmf{ "C" } );
      }

      delete( $fmf{ "C" } );
    }
    if ( exists( $fmf{ "F" } ) ) {  # Always put fluorine second
      $mass += $fmf{ "F" } * 18.99840316;
      if ( $fmf{ "F" } == 1 ) {
        $frag_mf .= "F";
      } else {
        $frag_mf .= sprintf( "F%d", $fmf{ "F" } );
      }

      delete( $fmf{ "F" } );
    }
    foreach my $e ( sort( keys( %fmf ) ) ) {  # Put remaining elements in alphabetic order
      if ( ! exists( $em{ $e } ) ) {
        die "Missing element \"$e\" mass for CID $cid\n";
      }
      $mass += $fmf{ $e } * $em{ $e };

      if ( $fmf{ $e } == 1 ) {
        $frag_mf .= $e;
      } else {
        $frag_mf .= sprintf( "%s%d", $e, $fmf{ $e } );
      }
    }
    $mffrags{ $ifrag }= $frag_mf;

    # Get fragment type
    my $frag_type = "singleton";
    if ( exists( $tfrags{ $ifrag } ) ) {
      $frag_type = $tfrags{ $ifrag };
    }

    if ( $nunsat != 0 ) {
      $frag_type = "unsaturated-" . $frag_type;
    }

    # Determine if fragment is whole molecule
#    my $molecule_f_count = $nfrag_c * 2 + 2 - 2 * $nunsat;
#    if ( $molecule_f_count == $nfrag_f ) {

    if ( $mf eq $frag_mf ) {
      $frag_type = "molecule-" . $frag_type;

      print STDERR "PFAS Fragment $ifrag MF: $frag_mf  Type: molecule  Frag Type: $frag_type  ",
                   "Unsaturation: $nunsat  Atoms: $nfrag_atoms  Bonds: $nfrag_bonds  ",
                   "Atoms: $nfrag_a  Fluorines: $nfrag_f\n";
      print TS2 "$cid",
	  "\t$frag_mf",
	  "\tmolecule",
	  "\t$frag_type",
	  "\t$nunsat",
	  "\t$nfrag_atoms",
	  "\t$nfrag_bonds",
	  "\t$nfrag_a",
	  "\t$nfrag_f",
	  "\t$mass",
          "\t$ms",
          "\t$mf",
#          "\t$flags",
#          "\t$oecd_atom",
#          "\t$not_oecd_atom",
          "\n";
    } else {
      print STDERR "PFAS Fragment $ifrag MF: $frag_mf  Type: fragment  Frag Type: $frag_type  ",
                   "Unsaturation: $nunsat  Atoms: $nfrag_atoms  Bonds: $nfrag_bonds  ",
                   "Atoms: $nfrag_a  Fluorines: $nfrag_f\n";
      print TS2 "$cid",
	  "\t$frag_mf",
	  "\tfragment",
	  "\t$frag_type",
	  "\t$nunsat",
	  "\t$nfrag_atoms",
	  "\t$nfrag_bonds",
	  "\t$nfrag_a",
	  "\t$nfrag_f",
	  "\t$mass",
          "\t$ms",
          "\t$mf",
#         "\t$flags",
#        "\t$oecd_atom",
#        "\t$not_oecd_atom",
        "\n";
    }
  }
 

  #
  # Determine the type of PFAS this is
  #
  my %oecd_coa = ();
  # Loop over atoms in the fragments and find connected atoms not in the fragments
  foreach my $ac ( keys( %apfas ) ) {
    foreach my $a ( keys( %{ $b{ $ac } } ) ) {  # Loop over connected atoms
      if ( ! exists( $apfas{ $a } ) ) {  # If not part of fragment, consider
        $oecd_coa{ $a }{ $ac } = undef;
      }
    }
  }

  # Detect ethers (and use to merge fragments)
  my $is_ether = 0;
  my @ca = keys( %oecd_coa );
  foreach my $ac ( @ca ) {
    my $at = substr( $l[ $ac + 3 ], 31, 2 );
    # Consider only attached oxygen atoms not attached to hydrogen and without a double bond
    if ( $at ne "O " || exists( $ach{ $at } ) || exists( $ua{ $at } ) ) { next; }  # Not an ether
print STDERR "Potential ether .0. CID $cid\n";

    my @at = keys( %{ $be{ $ac } } );
    my $nat = @at;
    if ( $nat != 2 ) { next; }  # Not an ether
print STDERR "Potential ether .1. CID $cid\n";

    if ( exists( $apfas{ $at[0] } ) && exists( $apfas{ $at[1] } ) ) {  # Ether joining two PFAS fragments
      my $frag_id = $apfas{ $at[0] };
      my $frag_id2 = $apfas{ $at[1] };

      $frags{ $frag_id }{ $ac } = undef;
      $afrags{ $frag_id }{ $ac } = undef;
      $apfas{ $ac } = $frag_id;
      $cfrags{ $ac } = 2;
      $cfrags{ $at[0] }++;
      $cfrags{ $at[1] }++;
      $bfrags{ $frag_id }++;
      $bfrags{ $frag_id }++;
      $is_ether++;
      delete( $oecd_coa{ $ac } );  # Don't consider this ether oxygen atom further

      if ( $frag_id == $frag_id2 ) {  # Special case .. both fragments already part of same fragment .. so cyclic
        $cyfrag{ $frag_id } = undef;
        $mffrags{ $frag_id } .= "-O-RING";

print STDERR "Ether frag: $mffrags{ $frag_id }\n";
      } else {
        # Merge the two fragments
        my @ap = keys( %{ $afrags{ $frag_id2 } } );
        foreach my $ap ( @ap ) {
          $frags{ $frag_id }{ $ap } = undef;
          $afrags{ $frag_id }{ $ap } = undef;
          $apfas{ $ap } = $frag_id;
        }
        $bfrags{ $frag_id } += $bfrags{ $frag_id2 };
        $sfrags{ $frag_id } += $sfrags{ $frag_id2 };
        if ( exists( $cyfrag{ $frag_id2 } ) ) { $cyfrag{ $frag_id } = undef; }

        # Need to merge tfrags data .. but how?
        $mffrags{ $frag_id } .= "-O-" . $mffrags{ $frag_id2 };
print STDERR "Ether frag before: $mffrags{ $frag_id }\n";
        $mffrags{ $frag_id } = join( "-O-", sort( split( "-O-", $mffrags{ $frag_id } ) ) );
print STDERR "Ether frag after: $mffrags{ $frag_id }\n";

        delete( $frags{ $frag_id2 } );
        delete( $afrags{ $frag_id2 } );
        delete( $bfrags{ $frag_id2 } );
        delete( $sfrags{ $frag_id2 } );
        delete( $tfrags{ $frag_id2 } );
        delete( $cyfrag{ $frag_id2 } );
        delete( $mffrags{ $frag_id2 } );

print STDERR "Ether frag: $mffrags{ $frag_id }\n";
      }
    } elsif ( ( ( exists( $ccacf{ $at[0] } ) && exists( $acf{ $at[0] } ) ) || exists( $apfas{ $at[0] } ) ) &&
              ( ( exists( $ccacf{ $at[1] } ) && exists( $acf{ $at[1] } ) ) || exists( $apfas{ $at[1] } ) ) ) {
      # Special case of candidate connecting atom with a fluorine connected
      # to an ether that becomes a PFAS atom
      my $frag_id = -1;
      my $ac1 = $at[0];
      if ( exists( $apfas{ $at[0] } ) ) {
        $frag_id = $apfas{ $at[0] };
      } else {  # examine the candidate to see what OECD fragment atom is attached to it
        foreach my $a ( keys( %{ $b{ $at[0] } } ) ) {  # Loop over connected atoms
          if ( exists( $apfas{ $a } ) ) {
            $frag_id = $apfas{ $a };
            $ac1 = $a;
            last;
          }
        }
      }

      my $frag_id2 = -1;
      my $ac2 = $at[1];
      if ( exists( $apfas{ $at[1] } ) ) {
        $frag_id2 = $apfas{ $at[1] };
      } else {  # examine the candidate to see what OECD fragment atom is attached to it
        foreach my $a ( keys( %{ $b{ $at[1] } } ) ) {  # Loop over connected atoms
          if ( exists( $apfas{ $a } ) ) {
            $frag_id2 = $apfas{ $a };
            $ac2 = $a;
            last;
          }
        }
      }

      if ( $frag_id == $frag_id2 ) {  # Special case .. both fragments already part of same fragment .. so cyclic
        $cyfrag{ $frag_id } = undef;

        # Add ether to PFAS fragment
        if ( $ac1 != $at[0] ) {
          $frags{ $frag_id }{ $at[0] } = undef;
          $afrags{ $frag_id }{ $at[0] } = undef;
          $apfas{ $at[0] } = $frag_id;
          $cfrags{ $at[0] } = 2;
          $bfrags{ $frag_id }++;
          delete( $oecd_coa{ $at[0] } );  # Don't consider this candidate PFAS atom further
        }

        if ( $ac2 != $at[1] ) {
          $frags{ $frag_id }{ $at[1] } = undef;
          $afrags{ $frag_id }{ $at[1] } = undef;
          $apfas{ $at[1] } = $frag_id;
          $cfrags{ $at[1] } = 2;
          $bfrags{ $frag_id }++;
          delete( $oecd_coa{ $at[1] } );  # Don't consider this candidate PFAS atom further
        }

        # Recompute the fragment MF
        my $tnc = 0;
        my $tnf = 0;
        foreach my $tat ( keys( %{ $frags{ $frag_id } } ) ) {
          $tnc++;
          if ( exists( $acf{ $tat } ) ) { $tnf += $acf{ $tat }; }
        }

        $mffrags{ $frag_id } = sprintf( "C%dF%d", $tnc, $tnf ) . "-O-RING";

        $frags{ $frag_id }{ $ac } = undef;
        $afrags{ $frag_id }{ $ac } = undef;
        $apfas{ $ac } = $frag_id;

        $bfrags{ $frag_id }++;
        $bfrags{ $frag_id }++;

        $cfrags{ $ac } = 2;
        $cfrags{ $ac1 }++;
        $cfrags{ $ac2 }++;

print STDERR "Ether frag: $mffrags{ $frag_id }\n";
      } else {
        # Adjust the fragments as needed
        if ( $ac1 != $at[0] ) {
          $cfrags{ $at[0] } = 2;
          $frags{ $frag_id }{ $at[0] } = undef;
          $afrags{ $frag_id }{ $at[0] } = undef;
          $apfas{ $at[0] } = $frag_id;
          $bfrags{ $frag_id }++;

          delete( $oecd_coa{ $ac } );  # Don't consider this candidate PFAS atom further

          # Recompute the fragment MF
          my $tnc = 0;
          my $tnf = 0;
          foreach my $tat ( keys( %{ $frags{ $frag_id } } ) ) {
            $tnc++;
            if ( exists( $acf{ $tat } ) ) { $tnf += $acf{ $tat }; }
          }

          $mffrags{ $frag_id } = sprintf( "C%dF%d", $tnc, $tnf );
        }

        if ( $ac2 != $at[1] ) {
          $cfrags{ $at[1] } = 2;
          $frags{ $frag_id2 }{ $at[1] } = undef;
          $afrags{ $frag_id2 }{ $at[1] } = undef;
          $apfas{ $at[1] } = $frag_id2;
          $bfrags{ $frag_id2 }++;

          # Recompute the fragment MF
          my $tnc = 0;
          my $tnf = 0;
          foreach my $tat ( keys( %{ $frags{ $frag_id2 } } ) ) {
            $tnc++;
            if ( exists( $acf{ $tat } ) ) { $tnf += $acf{ $tat }; }
          }

          $mffrags{ $frag_id2 } = sprintf( "C%dF%d", $tnc, $tnf );
        }


        # Merge the two fragments

        my @ap = keys( %{ $afrags{ $frag_id2 } } );
        foreach my $ap ( @ap ) {
          $frags{ $frag_id }{ $ap } = undef;
          $afrags{ $frag_id }{ $ap } = undef;
          $apfas{ $ap } = $frag_id;
        }
        $bfrags{ $frag_id } += $bfrags{ $frag_id2 };
        $sfrags{ $frag_id } += $sfrags{ $frag_id2 };
        if ( exists( $cyfrag{ $frag_id2 } ) ) { $cyfrag{ $frag_id } = undef; }

        # Need to merge tfrags data .. but how?
        $mffrags{ $frag_id } .= "-O-" . $mffrags{ $frag_id2 };
print STDERR "Ether frag before: $mffrags{ $frag_id }\n";
        $mffrags{ $frag_id } = join( "-O-", sort( split( "-O-", $mffrags{ $frag_id } ) ) );
print STDERR "Ether frag after: $mffrags{ $frag_id }\n";

        delete( $frags{ $frag_id2 } );
        delete( $afrags{ $frag_id2 } );
        delete( $bfrags{ $frag_id2 } );
        delete( $sfrags{ $frag_id2 } );
        delete( $tfrags{ $frag_id2 } );
        delete( $cyfrag{ $frag_id2 } );
        delete( $mffrags{ $frag_id2 } );
      }

      $frags{ $frag_id }{ $ac } = undef;
      $afrags{ $frag_id }{ $ac } = undef;
      $apfas{ $ac } = $frag_id;
      $cfrags{ $ac } = 2;
      $cfrags{ $ac1 }++;
      $cfrags{ $ac2 }++;
      $bfrags{ $frag_id }++;
      $bfrags{ $frag_id }++;
      $is_ether++;
      delete( $oecd_coa{ $ac } );  # Don't consider this ether oxygen atom further

    } elsif ( exists( $acf{ $ac } ) || exists( $acx{ $ac } ) ) {  # Only attached to one PFAS atom and a halogen
      my $s = "";  # Atomic symbol of attached halogen
      my $frag_id = 0;
      if ( exists( $apfas{ $at[0] } ) ) {
        $s = substr( $l[$at[1]+3], 31, 2 );  # Atomic symbol
        $cfrags{ $at[0] }++;
        $frag_id = $apfas{ $at[0] };
      } elsif ( exists( $apfas{ $at[1] } ) ) {
        $s = substr( $l[$at[0]+3], 31, 2 );  # Atomic symbol
        $cfrags{ $at[1] }++;
        $frag_id = $apfas{ $at[1] };
      } else {
        next;  # Not an ether (must be connected to one PFAS atom)
      }

      if ( substr( $s, 1, 1 ) eq " " ) { chop( $s ); }  # Remove trailing space

      # Add ether oxygen to PFAS fragment
      $apfas{ $ac } = $frag_id;
      $bfrags{ $frag_id }++;
      $frags{ $frag_id }{ $ac } = undef;
      $afrags{ $frag_id }{ $ac } = undef;
      $cfrags{ $ac } = 1;
      $mffrags{ $frag_id } .= "-O" . $s;

print STDERR "Ether halogen frag: $mffrags{ $frag_id }\n";
      delete( $oecd_coa{ $ac } );  # Don't consider this oxygen atom further
    } else {
      next;  # Not an ether
    }
  }

  if ( $is_ether != 0 ) {  print STDERR "Detected $is_ether perfluoroethers\n"; }


  # Give the perspective of the PFAS fragment (being attached to something else)
  my %pfas_at = ();
  foreach my $a1 ( keys( %apfas ) ) {
    my %al = ();
    foreach my $a2 ( keys( %{ $be{ $a1 } } ) ) {
      if ( exists( $apfas{ $a2 } ) ) {  # Attached to another PFAS atom?
        next;
      } elsif ( exists( $aa{ $a2 } ) ) {  # Attached to an aromatic atom?
        if ( exists( $al{ "AROMATIC" } ) ) {
          $al{ "AROMATIC" }++;
        } else {
          $al{ "AROMATIC" } = 1;
        }

        next;
      }

      my $s2 = substr( $l[$a2+3], 31, 2 );
      if ( substr( $s2, 1, 1 ) eq " " ) { chop( $s2 ); }  # Remove trailing space

      if ( $s2 eq "F" ) {  # Attached to fluorine
        next;
      } elsif ( $s2 eq "H" || $s2 eq "Cl" || $s2 eq "Br" || $s2 eq "I" ) {
        if ( exists( $al{ $s2 } ) ) {
          $al{ $s2 }++;
        } else {
          $al{ $s2 } = 1;
        }

        next;
      } else {
        if ( exists( $al{ "ALIPHATIC" } ) ) {
          $al{ "ALIPHATIC" }++;
        } else {
          $al{ "ALIPHATIC" } = 1;
        }
      }
    }

    my @al = sort( keys( %al ) );
    my $nal = @al;
    if ( $nal == 0 ) { next; }  # Only attached to PFAS and Fluorine

    my $al = "PFAS";
    my $aal = pop( @al );
    foreach my $sal ( @al ) {
      $al .= "(" . $sal . ")";

      my $nsal = $al{ $sal };
      if ( $nsal != 1 ) {
        $al .= sprintf( "%d", $nsal );
      }
    }

    my $naal = $al{ $aal };
    if ( $naal == 1 ) {
      $al .= "-" . $aal;
    } else {
      $al .= "-(" . $aal . ")" . sprintf( "%d", $naal );
    }

    my $frag_id = $apfas{ $a1 };
    if ( exists( $pfas_at{ $frag_id }{ $al } ) ) {
      $pfas_at{ $frag_id }{ $al }++;
    } else {
      $pfas_at{ $frag_id }{ $al } = 1;
    }
  }


  # Recompute connecting groups
  %oecd_coa = ();
  # Loop over atoms in the fragments and find connected atoms not in the fragments
  foreach my $ac ( keys( %apfas ) ) {
    foreach my $a ( keys( %{ $b{ $ac } } ) ) {  # Loop over connected atoms
      if ( ! exists( $apfas{ $a } ) ) {  # If not part of fragment, consider
        $oecd_coa{ $a }{ $ac } = undef;
      }
    }
  }

  # Give the perspective of the non-PFAS atom attached to the PFAS fragment
  my %pfas_ot = ();
  foreach my $a1 ( keys( %oecd_coa ) ) {
#    if ( exists( $aa{ $a1 } ) ) { next; }  # Is an aromatic atom?

    my $s1 = substr( $l[$a1+3], 31, 2 );
    if ( substr( $s1, 1, 1 ) eq " " ) { chop( $s1 ); }  # Remove trailing space


    my @frag_id = ();
    my %al = ();
    # Loop over atoms attached to this atom
    foreach my $a2 ( keys( %{ $be{ $a1 } } ) ) {
      if ( exists( $apfas{ $a2 } ) ) {  # Attached to a PFAS atom?
        push( @frag_id, $apfas{ $a2 } );  # Retain frag ID(s) that this atom is attached
        if ( exists( $al{ "PFAS" } ) ) {
          $al{ "PFAS" }++;
        } else {
          $al{ "PFAS" } = 1;
        }

        next;
      } elsif ( exists( $aa{ $a2 } ) ) {  # Attached to an aromatic atom?
        if ( exists( $al{ "AROMATIC" } ) ) {
          $al{ "AROMATIC" }++;
        } else {
          $al{ "AROMATIC" } = 1;
        }

        next;
      }

      my $bo = $be{ $a1 }{ $a2 };
      if ( $bo == 1 ) {
        $bo = "";
      } elsif ( $bo == 2 ) {
        $bo = "=";
      } elsif ( $bo == 1.5 ) {
        $bo = ":";
      } elsif ( $bo == 3 ) {
        $bo = "#";
      } else {
        print STDERR ":: ERROR :: Unrecognized bond order \"$bo\" for CID: $cid\n";
      }

      my $s2 = substr( $l[$a2+3], 31, 2 );
      if ( substr( $s2, 1, 1 ) eq " " ) { chop( $s2 ); }  # Remove trailing space

      # Check this atom to get hydrogen atom count and count of connections
      my %aa2a = ();
      foreach my $a3 ( keys( %{ $be{ $a2 } } ) ) {
        if ( $a3 == $a1 ) { next; }

        my $s3 = substr( $l[$a3+3], 31, 2 );
        if ( substr( $s3, 1, 1 ) eq " " ) { chop( $s3 ); }  # Remove trailing space
        if ( ! ( $s3 eq "H" || $s3 eq "F" || $s3 eq "Cl" || $s3 eq "Br" || $s3 eq "I" ) ) {
          $s3 = "R";
        }

        if ( exists( $aa2a{ $s3 } ) ) {
          $aa2a{ $s3 }++;
        } else {
          $aa2a{ $s3 } = 1;
        }
      }
      foreach my $aa2a ( sort( keys( %aa2a ) ) ) {
        $s2 .= $aa2a;
        my $naa2a = $aa2a{ $aa2a };
        if ( $naa2a > 1 ) { $s2 .= sprintf( "%d", $naa2a ); }
      }

      $bo .= $s2;

      if ( exists( $al{ $bo } ) ) {
        $al{ $bo }++;
      } else {
        $al{ $bo } = 1;
      }
    }


    my $al = "PFAS";
    my $nal = $al{ $al };
    $nal--;
    if ( $nal == 0 ) { delete( $al{ $al } ); } else { $al{ $al } = $nal; }
    $al .= "-" . $s1;

    if ( exists( $al{ "H" } ) ) {
      my $nsal = $al{ "H" };
      $al .= "H";
      if ( $nsal != 1 ) {
        $al .= sprintf( "%d", $nsal );
      }
      delete( $al{ "H" } );
    }

    my @al = sort( keys( %al ) );
    $nal = @al;
    if ( $nal != 0 ) {
      my $aal = "";
      my $naal = 0;
      if ( exists( $al{ "PFAS" } ) ) {
        $aal = "PFAS";
        $naal = $al{ $aal };
        delete( $al{ $aal } );
        @al = sort( keys( %al ) );
      } elsif ( exists( $al{ "AROMATIC" } ) ) {
        $aal = "AROMATIC";
        $naal = $al{ $aal };
        delete( $al{ $aal } );
        @al = sort( keys( %al ) );
      } else {
        $aal = pop( @al );
        $naal = $al{ $aal };
      }

      foreach my $sal ( @al ) {
        $al .= "(" . $sal . ")";

        my $nsal = $al{ $sal };
        if ( $nsal != 1 ) {
          $al .= sprintf( "%d", $nsal );
        }
      }

      if ( $naal == 1 ) {
        my $char = substr( $aal, 0, 1 );
        if ( $char eq "=" || $char eq "#" ) {
          $al .= $aal;
        } else {
          $al .= "-" . $aal;
        }
      } else {
        $al .= "(" . $aal . ")" . sprintf( "%d", $naal );
      }
    }


    foreach my $frag_id ( @frag_id ) {
      if ( exists( $pfas_ot{ $frag_id }{ $al } ) ) {
        $pfas_ot{ $frag_id }{ $al }++;
      } else {
        $pfas_ot{ $frag_id }{ $al } = 1;
      }
    }
  }


  my %sca = ();
  @ca = keys( %oecd_coa );
  foreach my $ca ( @ca ) {
    my $at = $aat{ $ca };
    if ( exists( $sca{ $at } ) ) {
      $sca{ $at }++;
    } else {
      $sca{ $at } = 1;
    }
  }
  my $sca = "";
  foreach my $at ( sort( keys( %sca ) ) ) {
    my $nat = $sca{ $at };
    if ( $nat != 1 ) {
      $sca .= sprintf( "%02dx%s", $nat, $at ) . ", ";
    } else {
      $sca .= $at . ", ";
    }
  }
  chop( $sca );
  chop( $sca );

  my $nca = @ca;
  if ( $nca == 0 ) {  # Whole molecule
    my %mf = ();
    foreach my $fid ( keys( %mffrags ) ) {
      my $mf = $mffrags{ $fid };
      if ( exists( $mf{ $mf } ) ) {
        $mf{ $mf }++;
      } else {
        $mf{ $mf } = 1;
      }
    }

    my @smf = sort( keys( %mf ) );
    my $n = @smf;
    if ( $n != 1 ) { print STDERR ":: ERROR :: MF count should be one but is \"$n\" .. 1\n"; }
    my $mf = $smf[0];
    $n = $mf{ $mf };
    if ( $n != 1 ) { print STDERR ":: ERROR :: MF count should be one but is \"$n\" .. 2\n"; }

    my %smf = ();
    foreach my $e ( split( /\d+|\-/, $mf ) ) {
      $smf{ $e } = undef;
    }
    my $smf = join( "", sort( keys( %smf ) ) );

    print TS4 "Whole\t$smf\t$mf\t$cid\n";
  } else {
    # Perspective of the PFAS fragment connected to a non-PFAS atom
    foreach my $frag_id ( keys( %pfas_at ) ) {
      my @pfas_at = sort( keys( %{ $pfas_at{ $frag_id } } ) );
      my $npfas_at = @pfas_at;
      my $spfas_at = "";
      foreach my $pat ( @pfas_at ) {
        my $npat = $pfas_at{ $frag_id }{ $pat };
        if ( $npat == 1 ) {
          $spfas_at .= $pat . ", ";
        } else {
          $spfas_at .= sprintf( "(%s)%d", $pat, $npat ) . ", ";
        }
      }
      chop( $spfas_at );
      chop( $spfas_at );

      # Perspective of the non-PFAS atom connected to PFAS fragment
      my @pfas_ot = sort( keys( %{ $pfas_ot{ $frag_id } } ) );
      my $spfas_ot = "";
      foreach my $pat ( @pfas_ot ) {
        my $npat = $pfas_ot{ $frag_id }{ $pat };
        if ( $npat == 1 ) {
          $spfas_ot .= $pat . ", ";
        } else {
          $spfas_ot .= "(" . $pat . ")" . sprintf( "%d", $npat ) . ", ";
        }
      }
      chop( $spfas_ot );
      chop( $spfas_ot );

      my $mf = $mffrags{ $frag_id };


#      if ( exists( $lut{ $spfas_ot } ) ) {
#        print TS4 "Overview of PFAS Groups\t$lut{$spfas_ot}\t$mf\t$cid\n";
#      } else {
        print TS4 "$spfas_at\t$spfas_ot\t$mf\t$cid\n";
#      }

#      if ( $spfas_at eq "PFAS-AROMATIC" ) {
#        print TS4 "Overview of PFAS Groups\tOther PFAS\tContains side-chain fluorinated aromatic",
#                  "\t$spfas_at\tContains $mf\t$cid\n";
#      } elsif ( $spfas_ot eq "PFAS-O-AROMATIC" ) {
#        print TS4 "Overview of PFAS Groups\tOther PFAS\tContains side-chain fluorinated aromatic",
#                  "\t$spfas_ot\tContains $mf\t$cid\n";
#
##PFAS-ALIPHATIC  PFAS-C(=O)-NR2  CF3
#      } elsif ( $spfas_ot eq "PFAS-C(=O)-NR2" ) {
#        print TS4 "Overview of PFAS Groups\tPFAA precursor\tContains PASF-based non-polymer",
#                  "\t$spfas_ot\tContains $mf\t$cid\n";
##PFAS-ALIPHATIC  PFAS-S(=O)2-AROMATIC    CF3
#      } elsif ( $spfas_ot eq "PFAS-S(=O)2-AROMATIC" ) {
#        print TS4 "Overview of PFAS Groups\tPFAA precursor\tContains PASF-based non-polymer",
#                  "\t$spfas_ot\tContains $mf\t$cid\n";
#      } else {
#        print TS4 "$spfas_at\t$spfas_ot\t$mf\t$cid\n";
#      }
    }


#    my $smf = "";
#    foreach my $mf ( sort( keys( %mf ) ) ) {
#      my $n = $mf{ $mf };
#      $smf .= sprintf( "%02dx%s", $n, $mf ) . ", ";
#    }
#    chop( $smf );
#    chop( $smf );

#    my $snca = sprintf( "%02d", $nca );
#    print TS4 "$spfas_ot\t$spfas_at\tPFAS fragments(s) attached to $snca atoms\tContains $smf\t$cid\n";
  }

if ( 1 ) {
  # Nothing more to do here
  $n = 0;
  @l = ();
  next; # go to next SDF record
}


  # Loop over connected atoms and figure out if important
#  @ca = keys( %oecd_coa );
#  my $nca = @ca;
  my $nua = keys( %ua );
  if ( $nca == 0 ) {
    my $ne = keys( %e );
    if ( $ne == 2 && exists( $e{ "F" } ) && exists( $e{ "C" } ) ) {
      if ( $nua == 0 ) {  # No double bonds
        print TS3 "Overview of PFAS Groups",
              "\tOther PFAS\tPerfluoroalkane\t$cid\n";
        print STDERR "  CID:$cid .. contains perfluoroalkane\n";
      } else {
        print TS3 "Overview of PFAS Groups",
              "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkene\t$cid\n";
        print STDERR "  CID:$cid .. contains perfluoroalkene\n";
      }
    } elsif ( $ne == 3 && exists( $e{ "F" } ) && exists( $e{ "C" } ) ) {
      if ( exists( $e{ "O" } ) ) {
        if ( $nua == 0 && $is_ether != 0 ) {  # No double bonds
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkylether\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkylether\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkylether Other\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkylether Other\n";
        }
      } else {
        print TS3 "Overview of PFAS Groups",
              "\tOther PFAS\tOther (None attached)\t$cid\n";
        print STDERR "  CID: $cid .. contains no connected other atoms .. ", join( " ", keys( %e ) ), "\n";
      }
    } else {
      print TS3 "Overview of PFAS Groups",
            "\tOther PFAS\tOther (None attached)\t$cid\n";
      print STDERR "  CID: $cid .. contains no connected other atoms .. ", join( " ", keys( %e ) ), "\n";
    }
  } else {
    # Create atoms types for each atom attached to a PFAS atom (but is not a PFAS atom)
    my %aat = ();
    my %cat = ();
    foreach my $ca ( @ca ) {
      my $at = substr( $l[ $ca + 3 ], 31, 2 );
      if ( substr( $at, 1, 1 ) eq " " ) { chop( $at ); }  # Remove trailing space
      if ( exists( $aa{ $ca } ) ) { $at =~ tr/A-Z/a-z/; }

      my %ta = ();
      foreach my $a2 ( keys( %{ $be{ $ca } } ) ) {
        my $s2 = substr( $l[$a2+3], 31, 2 );
        if ( substr( $s2, 1, 1 ) eq " " ) { chop( $s2 ); }  # Remove trailing space
        if ( exists( $aa{ $a2 } ) ) { $s2 =~ tr/A-Z/a-z/; }
      
        my $bo = $be{ $ca }{ $a2 };
        if ( $bo == 1 ) {
          $bo = "";
        } elsif ( $bo == 2 ) {
          $bo = "=";
        } elsif ( $bo == 1.5 ) {
          $bo = ":";
        } elsif ( $bo == 3 ) {
          $bo = "#";
        } else {
          print STDERR ":: ERROR :: Unrecognized bond order \"$bo\" for CID: $cid\n";
        }

        $bo .= $s2;

        if ( exists( $ta{ $bo } ) ) {
          $ta{ $bo }++;
        } else {
          $ta{ $bo } = 1;
        }
      }

      my @ta = sort( keys( %ta ) );
      my $ntta = @ta;
      if ( $ntta == 1 ) {
        my $nta = $ta{ $ta[0] };
        if ( $nta != 1 ) {
          $at .= "(" . $ta[0] . ")" . sprintf( "%d", $nta );
        } else {
          $at .= $ta[0];
        }
      } else {
        $ntta--;
        for ( my $ii = 0;  $ii < $ntta;  $ii++ ) {
          my $nta = $ta{ $ta[$ii] };
          if ( $nta != 1 ) {
            $at .= "(" . $ta[$ii] . ")" . sprintf( "%d", $nta );
          } else {
            $at .= "(" . $ta[$ii] . ")";
          }
        }
        my $nta = $ta{ $ta[$ntta] };
        if ( $nta != 1 ) {
          $at .= "(" . $ta[$ntta] . ")" . sprintf( "%d", $nta );
        } else {
          $at .= $ta[$ntta];
        }
      }

      if ( exists( $cat{ $at } ) ) {
        $cat{ $at }++;
      } else {
        $cat{ $at } = 1;
      }

      foreach my $pca ( keys( %{ $oecd_coa{ $ca } } ) ) {
        if ( exists( $aat{ $pca }{ $at } ) ) {
print STDERR "  CID $cid :: PFAS Atom ID $pca :: $at\n";
          $aat{ $pca }{ $at }++;
        } else {
print STDERR "  CID $cid :: PFAS Atom ID $pca :: $at\n";
          $aat{ $pca }{ $at } = 1;
        }
      }
    }

      my %sat = ();
      my @aat = keys( %aat );
      my $naat = @aat;
      foreach my $aat ( @aat ) {
        my $sat = "(";
        my @acat = sort( keys( %{ $aat{ $aat } } ) );
        my $nacat = @acat;
        foreach my $acat ( @acat ) {
          my $nacat = $aat{ $aat }{ $acat };
          if ( $nacat == 1 ) {
            $sat .= $acat . ", ";
          } else {
            $sat .= $aat{ $aat }{ $acat } . "x " . $acat . ", ";
          }
        }
        chop( $sat );
        chop( $sat );
        $sat .= ")";
        if ( exists( $sat{ $sat } ) ) {
          $sat{ $sat }++;
        } else {
          $sat{ $sat } = 1;
        }
      }

      my $nsat = keys( %sat );
      foreach my $sat ( sort( keys( %sat ) ) ) {
        if ( $sat{ $sat } == 1 ) {
          substr( $sat, 0, 1 ) = "";
          chop( $sat );
          print STDERR "  CID:$cid .$nsat. Atom type of PFAS connected atom: $sat\n";
        } else {
          print STDERR "  CID:$cid .$nsat. Atom type of PFAS connected atom: ", $sat{ $sat }, "x $sat\n";
        }
      }









    # Examine atom types and spot those with a known classification
    my $nat = keys( %cat );
    if ( $nat == 1 ) {  # Just one connected atom type
      my @cat = keys( %cat );
      if ( $cat[0] eq "C(=O)(C)O" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl carboxyic acid (PFCA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFCA\n";
          } elsif ( $na == 2 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl dicarboxyic acid (PFdiCA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFdiCA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl carboxyic acid (PFCA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFCA Other: ", $na, "x $cat[0]\n";
          }
        } else {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkylether carboxyic acid (PFECA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFECA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkylether carboxyic acid (PFECA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFECA Other: ", $na, "x $cat[0]\n";
          }
        }
      } elsif ( $cat[0] eq "S(=O)2(C)O" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfonic acid (PFSA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFSA\n";
          } elsif ( $na == 2 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane disulfonic acid (PFdiSA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFdiSA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfonic acid (PFSA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFSA Other: ", $na, "x $cat[0]\n";
          }
        } else {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkylether sulfonic acid (PFESA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFESA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkylether sulfonic acid (PFESA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFESA Other: ", $na, "x $cat[0]\n";
          }
        }
      } elsif ( $cat[0] eq "P(=O)(C)(O)2" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphonic acid (PFPA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PACF\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphonic acid (PFPA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFPA Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphonic acid (PFPA) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PFPA ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "P(=O)(C)2(O)" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphinic acid (PFPIA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFPIA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphinic acid (PFPIA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFPIA Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkyl phosphinic acid (PFPIA) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PFPIA ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(=O)(C)F" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkoyl fluorides (PACF)\t$cid\n";
            print STDERR "  CID:$cid .. contains PACF\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkoyl fluorides (PACF) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PACF Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkoyl fluorides (PACF) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PACF ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(=O)2(C)F" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkane sulfonyl fluorides (PASF)\t$cid\n";
            print STDERR "  CID:$cid .. contains PASF\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkane sulfonyl fluorides (PASF) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PASF Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkane sulfonyl fluorides (PASF) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PASF ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(=O)2(C)N" || $cat[0] eq "S(=O)2(C)c" || $cat[0] eq "S(=O)2(C)2" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPASF-based substance\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains PASF-based substance: ", $na, "x $cat[0]\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPASF-based substance Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains PASF-based substance Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(=O)(C)O" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfinic acid (PFSIA)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFSIA\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfinic acid (PFSIA) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFSIA Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA)\tPerfluoroalkane sulfinic acid (PFSIA) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PFSIA ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "O(C)H" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl alcohol\t$cid\n";
            print STDERR "  CID:$cid .. contains perfluoroalkyl alcohol\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl alcohol Other\t$cid\n";
            print STDERR "  CID:$cid .. contains perfluoroalkyl alcohol Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl alcohol Other\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkyl alcohol ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(C)(F)2I" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl iodide (PFAI)\t$cid\n";
            print STDERR "  CID:$cid .. contains PFAI\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl iodide (PFAI) Other\t$cid\n";
            print STDERR "  CID:$cid .. contains PFAI Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl iodide (PFAI) Other\t$cid\n";
          print STDERR "  CID:$cid .. contains PFAI ether Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(=O)(C)2" || $cat[0] eq "C(=O)(C)c" ) {
        my $na = $cat{ $cat[0] };
        my $ne = keys( %e );
        if ( $ne == 4 && $naa == 0 && $nua < 2
                      && exists( $e{ "F" } ) && exists( $e{ "C" } )
                      && exists( $e{ "O" } ) && exists( $e{ "H" } ) ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tSemi-fluorinated ketone\t$cid\n";
            print STDERR "  CID:$cid .. contains semi-fluorinated ketone\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tSemi-fluorinated ketone Other\t$cid\n";
            print STDERR "  CID:$cid .. contains semi-fluorinated ketone Other: ", $na, "x $cat[0]\n";
          }
        } elsif ( $ne == 3 && $naa == 0 && $nua < 2
                  && exists( $e{ "F" } ) && exists( $e{ "C" } )
                  && exists( $e{ "O" } ) ) {
          if ( $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl ketone\t$cid\n";
            print STDERR "  CID:$cid .. contains Perfluoroalkyl ketone: ", $na, "x $cat[0]\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tPerfluoroalkyl acid (PFAA) precursor\tPerfluoroalkyl ketone Other\t", $na, "x $cat[0]\t$cid\n";
            print STDERR "  CID:$cid .. contains Perfluoroalkyl ketone Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tSemi-fluorinated ketone\t$cid\n";
          print STDERR "  CID:$cid .. contains semi-fluorinated ketone Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "N(C)3" ) {
        my $na = $cat{ $cat[0] };
        my $ne = keys( %e );
        if ( $ne == 3 && $naa == 0 && $nua == 0 && $is_ether == 0 && $na == 1
             && exists( $e{ "F" } ) && exists( $e{ "C" } ) && exists( $e{ "N" } ) ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkyl-tert-amine\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkyl-tert-amine\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkyl-tert-amine Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkyl-tert-amine Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(C)2(F)c" || $cat[0] eq "n(:c)(:n)C" || $cat[0] eq "n(:c)2C" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic: ", $na, "x $cat[0]\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "O(C)c" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tEther linker\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ ether linker\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tEther linker Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ ether linker: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(C)c" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tThio-ether linker\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ thio-ether linker\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tThio-ether linker Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ thio-ether linker: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(=O)(C)c" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tSulfite linker\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ sulfite linker\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aromatic\tSulfite linker Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic w/ sulfite linker: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(=O)(C)N" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkyl amide\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkyl amide\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tPerfluoroalkyl amide Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains perfluoroalkyl amide Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "S(C)2" || $cat[0] eq "S(C)N" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic\tThio-ether linker\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic thio-ether linker: ", $na, "x $cat[0]\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic Other\tThio-ether linker\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic thio-ether linker Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "O(C)2" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic\tEther linker\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic ether linker: ", $na, "x $cat[0]\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic Other\tEther linker\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic ether linker Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(C)3H" || $cat[0] eq "C(C)3O" || $cat[0] eq "C(C)2(H)N"
                || $cat[0] eq "C(C)2(H)O" || $cat[0] eq "C(C)4" || $cat[0] eq "C(C)3N" 
                || $cat[0] eq "C(C)(H)(N)c" || $cat[0] eq "C(C)2(O)c" || $cat[0] eq "C(C)(H)2c"
                || $cat[0] eq "C(C)2(H)n" || $cat[0] eq "C(C)2(N)O" || $cat[0] eq "C(C)(H)(O)c" 
                || $cat[0] eq "C(C)2(H)c" || $cat[0] eq "C(C)3c" || $cat[0] eq "C(C)(H)2S"
                || $cat[0] eq "C(C)2(N)2" || $cat[0] eq "C(C)2(F)N" || $cat[0] eq "C(C)2(c)2"
                || $cat[0] eq "C(=C)(C)2" || $cat[0] eq "C(=C)(C)N" || $cat[0] eq "C(=N)(C)2" 
                || $cat[0] eq "C(=C)(C)H" || $cat[0] eq "C(C)2(N)c" || $cat[0] eq "C(=C)(C)c"
                || $cat[0] eq "C(C)3F" || $cat[0] eq "C(C)2(O)2" || $cat[0] eq "C(C)(O)(c)2"
                || $cat[0] eq "C(=C)(C)O" || $cat[0] eq "C(#C)C" || $cat[0] eq "C(C)(N)2c"
                || $cat[0] eq "C(=N)(C)N" || $cat[0] eq "C(C)2(H)S" || $cat[0] eq "C(=N)(C)c"
                || $cat[0] eq "C(=C)(C)F" || $cat[0] eq "C(=C)(C)Cl" || $cat[0] eq "C(C)2(F)O"
                || $cat[0] eq "C(C)2(F)H" || $cat[0] eq "N(C)2H" || $cat[0] eq "C(C)(H)(N)O"
                || $cat[0] eq "N(C)(H)N" || $cat[0] eq "N(C)2c" || $cat[0] eq "C(=N)(C)O"
                || $cat[0] eq "C(C)3n" || $cat[0] eq "C(Br)(C)2H" || $cat[0] eq "C(=N)(C)S"
                || $cat[0] eq "C(C)3S" || $cat[0] eq "C(C)(H)(N)2" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1 ) {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic: ", $na, "x $cat[0]\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tSide-chain fluorinated aliphatic Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains side-chain fluorinated aliphatic Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(C)(H)2N" || $cat[0] eq "C(C)(H)2O"
                || $cat[0] eq "C(C)(H)2n" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1) {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tN:1 fluorotelomere-based substance\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains n:1 fluorotelomere-based substance\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tN:1 fluorotelomere-based substance Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains n:1 fluorotelomere-based substance Other: ", $na, "x $cat[0]\n";
        }
      } elsif ( $cat[0] eq "C(C)2(H)2" ) {
        my $na = $cat{ $cat[0] };
        if ( $is_ether == 0 && $na == 1) {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tN:# fluorotelomere-based substance\t$cid\n";
          print STDERR "  CID:$cid .. contains n:# fluorotelomere-based substance\n";
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tPerfluoroalkyl acid (PFAA) precursor\tN:# fluorotelomere-based substance Other\t", $na, "x $cat[0]\t$cid\n";
          print STDERR "  CID:$cid .. contains n:# fluorotelomere-based substance Other: ", $na, "x $cat[0]\n";
        }
      } else {
        my @cat = keys( %cat );
        my $nlen = length( $cat[0] );
        my $na = $cat{ $cat[0] };
        if ( substr( $cat[0], 0, 1 ) eq "c" ) {
          if ( $is_ether == 0 && $na == 1 ) {
            print TS3 "Overview of PFAS Groups",
                  "\tOther PFAS\tSide-chain fluorinated aromatic\t", $na, "x $cat[0]\t$cid\n";
            print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic: ", $na, "x $cat[0]\n";
          } else {
            print TS3 "Overview of PFAS Groups",
                  "\tOther PFAS\tSide-chain fluorinated aromatic Other\t", $na, "x $cat[0]\t$cid\n";
            print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic Other: ", $na, "x $cat[0]\n";
          }
        } else {
          print TS3 "Overview of PFAS Groups",
                "\tOther PFAS\tOther (One attached)\t$cid\n";
          print STDERR "  CID:$cid .1. Atom type of PFAS connected atom: ",
                       $cat{ $cat[0] }, "x $cat[0]\n";
        }
      }
    } else {

      print TS3 "Overview of PFAS Groups",
            "\tOther PFAS\tOther ($nsat attached)\t$cid\n";

#      my $do_once1 = 1;
#      my $do_once2 = 0;
#      foreach my $cat ( keys( %cat ) ) {
#        if ( substr( $cat, 0, 1 ) eq "c" ) {
#          if ( $do_once1 ) {
#            $do_once1 = 0;
#            print TS3 "Overview of PFAS Groups",
#                  "\tOther PFAS\tSide-chain fluorinated aromatic\t$cid\n";
#            print STDERR "  CID:$cid .. contains side-chain fluorinated aromatic\n";
#          }
#        } else {
#          print STDERR "  CID:$cid .$nat. Atom type of PFAS connected atom: ", $cat{ $cat }, "x $cat\n";
#          $do_once2++;
#        }
#      }
#
#      if ( $do_once2 ) {
#        print TS3 "Overview of PFAS Groups",
#              "\tOther PFAS\tOther ($nat attached)\t$cid\n";
#      }
    }
  }


  # Nothing more to do here
  $n = 0;
  @l = ();
  next; # go to next SDF record
}

close( TS1 );
close( TS2 );
close( TS3 );

print STDERR "Processing complete\n";


sub bynum { $a <=> $b };

