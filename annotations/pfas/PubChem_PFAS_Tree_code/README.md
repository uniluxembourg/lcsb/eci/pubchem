# Constructing the PubChem PFAS Tree

This file demonstrates how to use the scripts in this folder to construct the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120).
It assumes one is using Linux with access to gzip, cat, zcat, tail, grep binaries.

## Step 1  
Download the complete set of sdf.gz files from the PubChem FTP site 
from:  https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/CURRENT-Full/SDF/

Also download the files:

- https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Component.gz
- https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Parent.gz

## Step 2
Use these two scripts to download the trees needed to produce the PFAS list data
For example:
```
( ./get_pfas_lists.pl 1 > get_pfas_lists.out ) >& get_pfas_lists.err
( ./get_pfas_reg_lists.pl 1 > get_pfas_reg_lists.out ) >& get_pfas_reg_lists.err
```

## Step 3
For each downloaded sdf.gz file (337 files as of June 12, 2023), process as such 
(where "001" is changed for each file), e.g.:
```
(  ./get_cf2_csv3.pl Compound_000000001_000500000.sdf.gz pfas_001.tsv ) >& pfas_001.err
```

## Step 4
Ensure each file processing completes succesfully, such that the last line of the 
error file contains "Processing complete", e.g.:
```
tail -n1 pfas_*.err | grep "Processing complete" | wc -l
```

## Step 5
After all downloaded sdf.gz files are processed, merge the contents of the output files, 
e.g.:
```
  cat tfarm_oecd/*.tsv1 | gzip > pfas_frag2_set1.tsv.gz
  cat tfarm_oecd/*.tsv2 | gzip > pfas_frag2_set2.tsv.gz
  cat tfarm_oecd/*.tsv4 | gzip > pfas_frag2_set4.tsv.gz
``` 

## Step 6
Generate the OECD tree, e.g.:
```
./analyze_pfas_frag_set.pl
```

## Step 7
Generate the breakdowns by chemistry tree, e.g.:
```
( ./summarize_pfas_functional_class_by_fragment_pt1.pl pfas_frag2_set4.tsv.gz | gzip > treeatomenv_initial.tsv.gz ) >& tmp_tree.err
zcat treeatomenv_initial.tsv.gz | sort --parallel=24 --buffer-size=64G | gzip > treeatomenv_sort.tsv.gz
( ./summarize_pfas_functional_class_by_fragment_pt2.pl treeatomenv_sort.tsv.gz | gzip > treeatomenv_sort.tsv.gz2 ) >& tmp_tree.err2
( ./summarize_pfas_functional_class_by_fragment_pt3.pl treeatomenv_sort.tsv.gz2 | gzip > treeatomenv.tsv.gz ) >& tmp_tree.err3
```
The outputs are then ready for processing into the final product, e.g.:
https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120

## Credits 
Author (DoThis.sh) Evan Bolton (NCBI/NLM/NIH).
Edited (into README.md) Emma Schymanski (LCSB/UniLu). 
