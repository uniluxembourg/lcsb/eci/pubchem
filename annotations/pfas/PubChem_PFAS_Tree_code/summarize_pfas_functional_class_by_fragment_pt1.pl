#!/usr/bin/perl

use strict;
use warnings;


#
#PFAS-AROMATIC   PFAS-C(AROMATIC)2       CF3     12889
#PFAS-ALIPHATIC  PFAS-C(NR2)(OH)-PFAS    CF3     13045
#PFAS-ALIPHATIC  PFAS-C(NR2)(OH)-PFAS    CF3     13045
#(PFAS-ALIPHATIC)2       (PFAS-CH=CHR)2  C4F8    13054
#PFAS-AROMATIC   PFAS-C(AROMATIC)2       CF3     13095
#PFAS-ALIPHATIC  PFAS-C(SR)2-PFAS        CF3     13098
#
#PFAS-ALIPHATIC  PFAS-C(=O)-CHR2 CF3     1314
#PFAS-(ALIPHATIC)2       PFAS-CH2-CH2R, PFAS-P(=O)(OH)-OR        CF2     1315
#PFAS-(ALIPHATIC)2       PFAS-CH2-CH2R, PFAS-P(=O)(OH)2  CF2     1315
#PFAS-AROMATIC   PFAS-C(AROMATIC)2       CF3     1359
#PFAS-ALIPHATIC  PFAS-CH2-OR     CF3     1381
#PFAS-AROMATIC   PFAS-C(AROMATIC)2       CF3     1396
#PFAS-(ALIPHATIC)2       PFAS-C(=O)-CHR2, PFAS-C(=O)-NHR CF2     1525
#


# Parameters
my $cfile = "CID-Component.gz";  # Component file
my $pfile = "CID-Parent.gz";  # Salt file


# Read in commandline info
my $file = $ARGV[0];
if ( ! defined( $file ) || $file eq "" ) {
  die "Please specify a gz compressed file to operate on\n";
}
print STDERR "Reading file: $file\n";


#
# Read the data file to get unique set of CIDs (to efficiently read salt/mixture information)
#
my $nl = 0;  # Count of lines in file
my %c = ();  # Unique set of CIDs
open( TSV, "gunzip < $file |" ) || die "Unable to read file: $file\n";
while ( $_ = <TSV> ) {
  chop;
  my @tmp = split( /	/, $_, 4 );

  if ( ! defined( $tmp[2] ) || $tmp[2] eq "" ) {
#    print STDERR ":: WARNING :: MF is empty .. \"$_\"\n";
    next;  # Bug in the fragment merge procedure .. skip for now
  }

  $nl++;
  $c{ $tmp[3] } = undef;  # get the unique set of CIDs
}
close( TSV );
my $ncid = keys( %c );
print STDERR "Read $nl lines and found $ncid unique CIDs\n\n";


#
# Get the list of parents/salts
#
print STDERR "Reading parent file \"$pfile\"\n";
$nl = 0;  # Count of lines in file
my $nsal = 0;  # Count of salts
my $nrem = 0;  # Count of cases of removed charged species
my %cm = ();  # Cases of component mixtures (component CID -> mixture CIDs)
open( PAR, "gunzip < $pfile |" ) || die "Unable to read file: $pfile\n";
while ( $_ = <PAR> ) {
  chop;
  $nl++;
  my ( $cid, $par ) = split( /	/, $_, 2 );

  # Ignore self parents
  if ( $cid == $par ) { next; }

  # Remove cases of charged species in CID list
  if ( exists( $c{ $cid } ) ) {  # Charged form as has a non-self parent
    delete( $c{ $cid } );
    $c{ $par } = undef;
    $cm{ $par }{ $cid } = undef;
    $nsal++;
    $nrem++;
  } elsif ( exists( $c{ $par } ) ) {
    $cm{ $par }{ $cid } = undef;
    $nsal++;
  }
}
close( PAR );
my $npar = keys( %cm );  # Count of parent CIDs
$ncid = keys( %c );  # Count of CIDs (now adjusted for charged form removal)
print STDERR "Read $nl lines and found $npar parent CIDs with $nsal salts but removed $nrem salts from PFAS neutral set (now $ncid neutral CIDs)\n\n";


#
# Get the list of mixtures/components
#
$nl = 0;  # Count of lines in file
$nsal = 0;  # Count of salts/mixtures
$nrem = 0;  # Count of removed mixtures from original CID list
print STDERR "Reading component file \"$cfile\"\n";
open( MIX, "gunzip < $cfile |" ) || die "Unable to read file: $cfile\n";
while ( $_ = <MIX> ) {
  chop;
  $nl++;

  my ( $mix, @c ) = split( /	/, $_ );
  my $nc = @c;
  if ( $nc == 0 ) {
    print STDERR ":: WARNING :: Line $nl :: CID $mix has no component CIDs!\n";
  }

  if ( exists( $c{ $mix } ) ) {  # Mixture CID is in the CID list
    if ( $nc == 1 ) {  # Salt
      delete( $c{ $mix } );
      $c{ $c[0] } = undef;
      $cm{ $c[0] }{ $mix } = undef;
      $nrem++;
      $nsal++;
    } else {
      print STDERR ":: WARNING :: Line $nl :: CID $mix is a mixture and in the PFAS Neutral list!  \"$_\"\n";
    }
  } else {
    # Examine Component CIDs
    foreach my $c ( @c ) {
      if ( $c == $mix ) {
#        print STDERR ":: WARNING :: CID $mix is both a component and a mixture!  \"$_\"\n";
        next;
      } elsif ( exists( $c{ $c } ) ) {  # Component CID is in the CID list
        $cm{ $c }{ $mix } = undef;
      } # Else ignore mixture CID that is not a PFAS
    }
  }
}
close( MIX );
$ncid = keys( %c );  # Count of CIDs (now adjusted for salt form removal)
my $ncom = keys( %cm );
print STDERR "Read $nl lines and found $ncom neutral PFAS w/ mixtures or salts (now $ncid neutral CIDs)\n\n";


#
# Validation check that parents are not salt/mixtures and vice-versa
#
my $ns = 0;
foreach my $c ( keys( %c ) ) {
  if ( exists( $cm{ $c } ) ) {
    $ns++;
  }
}
print STDERR "There are $ns out of $ncid CIDs with a salt/mixture (vs. $ncom)\n";

my $nm = 0;
foreach my $c ( keys( %cm ) ) {
  foreach my $m ( keys( %{ $cm{ $c } } ) ) {
    if ( exists( $c{ $m } ) ) {
      $nm++;
    }
  }
}
print STDERR "There are $nm salt/mixture CIDs (non-unique) that are also a neutral\n";


#
# Read the main data file, again (this time with all necessary info available)
#
#PFAS-AROMATIC   PFAS-C(AROMATIC)2       CF3     12889
#PFAS-ALIPHATIC  PFAS-C(NR2)(OH)-PFAS    CF3     13045
#PFAS-ALIPHATIC  PFAS-C(NR2)(OH)-PFAS    CF3     13045
#

my $l = "PFAS breakdowns by chemistry";

my $l_nm = "Breakdown by PFAS composition\tNeutral";
my $l_sm = "Breakdown by PFAS composition\tSalt/Mixture";


open( TSV, "gunzip < $file |" ) || die "Unable to read file: $file\n";
while ( $_ = <TSV> ) {
  chop;
  my @tmp = split( /	/, $_, 4 );

  #
  # Check applicability of the line (covered or not)
  #
  if ( ! defined( $tmp[2] ) || $tmp[2] eq "" ) {
#    print STDERR ":: WARNING :: MF is empty .. \"$_\"\n";
    next;  # Bug in the fragment merge procedure .. skip for now
  } elsif ( ! exists( $c{ $tmp[3] } ) ) {
    next;  # CID must be a parent CID, else ignore
  } elsif ( $tmp[0] eq "Whole" ) {
    next;  # Skip whole molecule CIDs for the moment
  }


  #
  # Get the salt/mixture for this CID
  #
  my $pcid = $tmp[3];  # Parent CID

  my @mix_cid = ();  # Set of mixture CIDs for a given parent CID
  if ( exists( $cm{ $pcid } ) ) {
    @mix_cid = sort bynum( keys( %{ $cm{ $pcid } } ) );
  }


  #
  # Determine the fragment connectivity degree
  #
  my $l_fc = $tmp[0];  # Fragment connectivity data
  $l_fc =~ s/ALIPHATIC/Aliphatic/g;
  $l_fc =~ s/AROMATIC/Aromatic/g;
  if ( $l_fc eq "PFAS-Aliphatic" ||
       $l_fc eq "PFAS-Aromatic" ||
       $l_fc eq "PFAS-Cl" ) {
    $l_fc = "Breakdown by PFAS part connectivity degree\tPFAS parts with 1 connection\t" . $l_fc
  } else {
    $l_fc = "Breakdown by PFAS part connectivity degree\tPFAS parts with 2+ connections\t" . $l_fc;
  }


  #
  # Do the Molecular Formula and Ether handling
  #
  my %l_mf = ();  # Labels for molecular formula
  my $l_mf = $tmp[2];  # PFAS Molecular formula for CID
  if ( $l_mf =~ /\-/ ) {
    if ( $l_mf =~ /\-O\-/ ) {
      my @f = split( /\-O\-/, $l_mf );  # Break up ether MF into component pieces
      foreach my $f ( @f ) {
        if ( $f eq "" || $f eq "RING" ) { next; }  # Skip special cases

        if ( $f =~ /\-/ ) {
          my @e = split( /\-/, $f, 2 );
          $f = shift( @e );
        }

        if ( $f =~ /C\d+F\d+/ ) {
          $f =~ /C(\d+)F(\d+)/;
          my $nc = $1;
          my $nf = $2;
          $f = sprintf( "C%02dF%02d", $nc, $nf );
        } elsif ( $f =~ /CF\d+/ ) {
          $f =~ /CF(\d+)/;
          my $nc = 1;
          my $nf = $1;
          $f = sprintf( "C%02dF%02d", $nc, $nf );
        } else {
          print STDERR ":: WARNING :: Strange MF case: $f\n";
        }

        my $el = "Breakdown by PFAS part formulas\tMolecule contains " . $f . "\tPFAS ethers containing " . $f . "\t" . $l_mf;
        $l_mf{ $el } = undef;
      }
    } else {
      my ( @f ) = split( /\-/, $l_mf );  # Break up ether MF into component pieces
      my $f = shift( @f );

      if ( $f =~ /C\d+F\d+/ ) {
        $f =~ /C(\d+)F(\d+)/;
        my $nc = $1;
        my $nf = $2;
        $f = sprintf( "C%02dF%02d", $nc, $nf );
      } elsif ( $f =~ /CF\d+/ ) {
        $f =~ /CF(\d+)/;
        my $nc = 1;
        my $nf = $1;
        $f = sprintf( "C%02dF%02d", $nc, $nf );
      } else {
        print STDERR ":: WARNING :: Strange MF case: $f\n";
      }

      my $el = "Breakdown by PFAS part formulas\tMolecule contains " . $f . "\tPFAS ethers containing " . $f . "\t" . $l_mf;
      $l_mf{ $el } = undef;
    }
  } else {  # Not an ether
    my $f = $l_mf;
    if ( $f =~ /C\d+F\d+/ ) {
      $f =~ /C(\d+)F(\d+)/;
      my $nc = $1;
      my $nf = $2;
      $f = sprintf( "C%02dF%02d", $nc, $nf );
    } elsif ( $f =~ /CF\d+/ ) {
      $f =~ /CF(\d+)/;
      my $nc = 1;
      my $nf = $1;
      $f = sprintf( "C%02dF%02d", $nc, $nf );
    } else {
      print STDERR ":: WARNING :: Strange MF case: $f\n";
    }

    $l_mf = "Breakdown by PFAS part formulas\tMolecule contains " . $f;
    $l_mf{ $l_mf } = undef;
  }


  #
  # Get the set of fragments for this CID
  #
  my %l_ae = ();  # Labels for atom environment
  my @frag = ();
  if ( $tmp[1] =~ /\,/ ) {
    @frag = split( /\, /, $tmp[1] );
  } else {
    push( @frag, $tmp[1] );
  }

  # Work on each fragment atom environment to assign element and type
  foreach my $frag ( @frag ) {
    # determine the PFAS type .. in a crude sense
    my $type = "";
    if ( $frag =~ /PFAS-C/ &&
         ! ( $frag =~ /PFAS-Cl/ ) ) {  # 2,545,362 of 6,905,102
      if ( $frag =~ /PFAS-C\(=O\)/ ) {
        $type = "Contains PFAS-C\tContains PFAS-C(=O)";
      } elsif ( $frag =~ /PFAS-C\(=C/ ) {
        $type = "Contains PFAS-C\tContains PFAS-C(=C";
      } elsif ( $frag =~ /PFAS-C\(=N/ ) {
        $type = "Contains PFAS-C\tContains PFAS-C(=N";
      } elsif ( $frag =~ /PFAS-C\(=S/ ) {
        $type = "Contains PFAS-C\tContains PFAS-C(=S";
      } elsif ( $frag =~ /PFAS-C\(=/ ) {
        $type = "Contains PFAS-C\tMore PFAS-C cases";
      } elsif ( $frag =~ /PFAS-C\(/ ) {
        $type = "Contains PFAS-C\tContains PFAS-C(";
      } elsif ( $frag =~ /PFAS-CH3/ ) {
        $type = "Contains PFAS-C\tContains PFAS-CH3";
      } elsif ( $frag =~ /PFAS-CH2/ ) {
        $type = "Contains PFAS-C\tContains PFAS-CH2";
      } elsif ( $frag =~ /PFAS-CH/ ) {
        $type = "Contains PFAS-C\tContains PFAS-CH";
      } else {
        $type = "Contains PFAS-C\tMore PFAS-C cases";
      }

    } elsif ( $frag =~ /PFAS-O/ ) {  # 587,211 of 6,905,102
      if ( $frag =~ /PFAS-O-/ ) {
        $type = "Contains PFAS-O\tContains PFAS-O-R";
      } elsif ( $frag =~ /PFAS-OH/ ) {
        $type = "Contains PFAS-O\tContains PFAS-OH";
      } else {
        $type = "Contains PFAS-O\tMore PFAS-O cases";
      }

    } elsif ( $frag =~ /PFAS-S/ ) {  # 216,302 of 6,905,102
      if ( $frag =~ /PFAS-S\(=O\)/ ) {
        $type = "Contains PFAS-S\tContains PFAS-S(=O)";
      } elsif ( $frag =~ /PFAS-S\(=N/ ) {
        $type = "Contains PFAS-S\tContains PFAS-S(=N";
      } elsif ( $frag =~ /PFAS-S\(=C/ ) {
        $type = "Contains PFAS-S\tContains PFAS-S(=C";
      } elsif ( $frag =~ /PFAS-S\(/ ) {
        $type = "Contains PFAS-S\tMore PFAS-S cases";
      } elsif ( $frag =~ /PFAS-S-/ ) {
        $type = "Contains PFAS-S\tContains PFAS-S-R";
      } elsif ( $frag =~ /PFAS-SH/ ) {
        $type = "Contains PFAS-S\tContains PFAS-SH";
      } elsif ( $frag =~ /PFAS-Si/ ) {
        $type = "More PFAS-Element cases\tContains PFAS-Si";
      } elsif ( $frag =~ /PFAS-Se/ ) {
        $type = "More PFAS-Element cases\tContains PFAS-Se";
      } elsif ( $frag =~ /PFAS-Sn/ ) {
        $type = "More PFAS-Element cases\tContains PFAS-Sn";
      } elsif ( $frag =~ /PFAS-Sb/ ) {
        $type = "More PFAS-Element cases\tContains PFAS-Sb";
      } else {
        $type = "Contains PFAS-S\tMore PFAS-S cases";
      }

    } elsif ( $frag =~ /PFAS-N/ ) {  # 36,534 of 6,905,102
      if ( $frag =~ /PFAS-N\(=/ ) {
        $type = "Contains PFAS-N\tContains PFAS-N(=R";
      } elsif ( $frag =~ /PFAS-N\(/ ) {
        $type = "Contains PFAS-N\tContains PFAS-N(";
      } elsif ( $frag =~ /PFAS-NH3/ ) {
        $type = "Contains PFAS-N\tContains PFAS-NH3";
      } elsif ( $frag =~ /PFAS-NH2/ ) {
        $type = "Contains PFAS-N\tContains PFAS-NH2";
      } elsif ( $frag =~ /PFAS-NH/ ) {
        $type = "Contains PFAS-N\tContains PFAS-NH";
      } elsif ( $frag =~ /PFAS-N=/ ) {
        $type = "Contains PFAS-N\tContains PFAS-N=R";
      } elsif ( $frag =~ /PFAS-N-/ ) {
        $type = "Contains PFAS-N\tContains PFAS-N-R";
      } else {
        $type = "Contains PFAS-N\tMore PFAS-N cases";
      }

    } elsif ( $frag =~ /PFAS-P/ ) {  # 14,435 of 6,905,102
      if ( $frag =~ /PFAS-P\(=O\)/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P(=O)";
      } elsif ( $frag =~ /PFAS-P\(=S/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P(=S";
      } elsif ( $frag =~ /PFAS-P\(=N/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P(=N";
      } elsif ( $frag =~ /PFAS-P\(=C/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P(=C";
      } elsif ( $frag =~ /PFAS-P\(=/ ) {
        $type = "Contains PFAS-P\tMore PFAS-P cases";
      } elsif ( $frag =~ /PFAS-P=/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P=R";
      } elsif ( $frag =~ /PFAS-P\(/ ) {
        $type = "Contains PFAS-P\tContains PFAS-P(";
      } elsif ( $frag =~ /PFAS-PH2/ ) {
        $type = "Contains PFAS-P\tContains PFAS-PH2";
      } elsif ( $frag =~ /PFAS-PH/ ) {
        $type = "Contains PFAS-P\tContains PFAS-PH";
      } else {
        $type = "Contains PFAS-P\tMore PFAS-P cases";
      }

    } elsif ( $frag =~ /PFAS-Bi/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Bi";
    } elsif ( $frag =~ /PFAS-Br/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Br";
    } elsif ( $frag =~ /PFAS-As/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-As";
    } elsif ( $frag =~ /PFAS-Te/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Te";
    } elsif ( $frag =~ /PFAS-Hg/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Hg";
    } elsif ( $frag =~ /PFAS-Ga/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Ga";
    } elsif ( $frag =~ /PFAS-B/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-B";
    } elsif ( $frag =~ /PFAS-Ge/ ) {
      $type = "More PFAS-Element cases\tContains PFAS-Ge";
    } else {
      $type = "More PFAS-Element cases\tYet more PFAS-Element cases";
    }

    # create the atom environment labels
    my $ae = "Breakdown by PFAS functional groups\t" . $type . "\t" . $frag;
    $l_ae{ $ae } = undef;
  }


  # my $l_nm = "Breakdown by PFAS composition\tNeutral";
  # my $l_sm = "Breakdown by PFAS composition\tSalt/Mixture";
  # my $l_fc  # Fragment connectivity data
  # my $pcid  # Parent CID
  # my @mix_cid = ();  # Set of mixture CIDs for a given parent CID
  # my %l_nm = ();  # Labels for neutral/mixture
  # my %l_ae = ();  # Labels for atom environment
  # my %l_mf = ();  # Labels for molecular formula
  # my %l_fc = ();  # Labels for fragment connectivity degree


  #
  # Output the tree
  #
  my @l_mf = sort( keys( %l_mf ) );
  my @l_ae = sort( keys( %l_ae ) );


  #
  # Composition
  #
  print "$l\t$l_nm\t$l_fc\t$pcid\n";
  foreach my $ae ( @l_ae ) {
    print "$l\t$l_nm\t$ae\t$pcid\n";
  }
  foreach my $mf ( @l_mf ) {
    print "$l\t$l_nm\t$mf\t$pcid\n";
  }

  foreach my $cid ( @mix_cid ) {  # Salts/Mixtures
    print "$l\t$l_sm\t$l_fc\t$cid\n";
    foreach my $ae ( @l_ae ) {
      print "$l\t$l_sm\t$ae\t$cid\n";
    }
    foreach my $mf ( @l_mf ) {
      print "$l\t$l_sm\t$mf\t$cid\n";
    }
  }


  #
  # Connectivity Degree
  #
  print "$l\t$l_fc\t$l_nm\t$pcid\n";
  foreach my $ae ( @l_ae ) {
    print "$l\t$l_fc\t$ae\t$pcid\n";
  }
  foreach my $mf ( @l_mf ) {
    print "$l\t$l_fc\t$mf\t$pcid\n";
  }

  foreach my $cid ( @mix_cid ) {  # Salts/Mixtures
    print "$l\t$l_fc\t$l_sm\t$cid\n";
    foreach my $ae ( @l_ae ) {
      print "$l\t$l_fc\t$ae\t$cid\n";
    }
    foreach my $mf ( @l_mf ) {
      print "$l\t$l_fc\t$mf\t$cid\n";
    }
  }


  #
  # Atom Environment
  #
  foreach my $ae ( @l_ae ) {
    print "$l\t$ae\t$l_nm\t$pcid\n";
    print "$l\t$ae\t$l_fc\t$pcid\n";

    foreach my $mf ( @l_mf ) {
      print "$l\t$ae\t$mf\t$pcid\n";
    }

    foreach my $cid ( @mix_cid ) {  # Salts/Mixtures
      print "$l\t$ae\t$l_sm\t$cid\n";
      print "$l\t$ae\t$l_fc\t$cid\n";

      foreach my $mf ( @l_mf ) {
        print "$l\t$ae\t$mf\t$cid\n";
      }
    }
  }

  #
  # Molecular Formula
  #
  foreach my $mf ( @l_mf ) {
    print "$l\t$mf\t$l_nm\t$pcid\n";
    print "$l\t$mf\t$l_fc\t$pcid\n";

    foreach my $ae ( @l_ae ) {
      print "$l\t$mf\t$ae\t$pcid\n";
    }

    foreach my $cid ( @mix_cid ) {  # Salts/Mixtures
      print "$l\t$mf\t$l_sm\t$cid\n";
      print "$l\t$mf\t$l_fc\t$cid\n";

      foreach my $ae ( @l_ae ) {
        print "$l\t$mf\t$ae\t$cid\n";
      }
    }
  }
}
close( TSV );


sub bynum { $b <=> $a };

