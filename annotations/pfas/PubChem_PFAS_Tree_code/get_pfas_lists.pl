#!/usr/bin/perl
#

use strict;
use warnings;
use Time::HiRes;

my $download = $ARGV[0];
if ( ! defined( $download ) || $download != 1 ) {
  $download = 0;
}

if ( $download ) {
  my $dl_url = "https://gitlab.lcsb.uni.lu/eci/pubchem/-/raw/master/annotations/pfas/pfas_tree_node_description_static.txt?inline=false";
  my $command = "/usr/bin/wget --tries=10 --waitretry=1 -O treenodedescriptionstatic.tsv $dl_url";
  print "$command\n";
  print `$command`, "\n";
}

my $ifile = "PFAS_List_of_Lists.txt";
if ( $download ) {
  my $dl_url = "https://gitlab.lcsb.uni.lu/eci/pubchem/-/raw/master/annotations/pfas/" . $ifile . "?inline=false";
  my $command = "/usr/bin/wget --tries=10 --waitretry=1 -O $ifile $dl_url";
  print "$command\n";
  print `$command`, "\n";
}

if ( ! ( -e "./treelists" ) ) {
  print `/usr/bin/mkdir ./treelists`, "\n";
}


my $nl = 0;
if ( $download ) {
  open( CSV, "$ifile" ) || die "Unable to read: $ifile\n";
#  my $header = <CSV>;
#Entry       Title   ToolTip Hierarchy       SubsetOfEntry SourceHid        nodeHNID        LinkToCIDs      n_CIDs  LinkOutURL
#  print STDERR ":: WARNING :: Skipped header line in file \"$ifile\": $header";
  my $n = 0;
  my @f = ();
  my @n = ();
  my @d = ();
  my @u = ();
  my $nfl = 0;
  while ( $_ = <CSV> ) {
    chop;
    $nfl++;

#  my @tmp = split( /	/, $_ );
#  my $ntmp = @tmp;
#  for ( my $i = 0;  $i < $ntmp;  $i++ ) {
#    print "$i :: $tmp[$i]\n";
#  }
#  next;

    my @tmp = split( /	/, $_, 10 );
    if ( ! ( defined( $tmp[7] ) && $tmp[7] ne "" ) ) { next; }

    if ( $nl == 0 && defined( $tmp[0] ) && $tmp[0] eq "Entry" ) { next; }
    $nl++;

    my @tmp2 = split( /\//, $tmp[7] );

    my $file = "";

    if ( $tmp2[2] eq "pubchem.ncbi.nlm.nih.gov" ) {
      $file = "./treelists/tree_" . $tmp2[6] . "_" . $tmp2[7] . "_" . $tmp2[8] . ".txt";
    } else {
      $file = "./treelists/tree_" . pop( @tmp2 );
    }

    $f[ $n ] = $file;
    $n[ $n ] = $tmp[8];
    $d[ $n ] = $tmp[1];
    $n++;

    my $command = "/usr/bin/wget --tries=10 --waitretry=1 -O $file $tmp[7]";
    print "$command\n";
    print `$command`, "\n";
#  print "\"$tmp[7]\"\n";
    Time::HiRes::sleep( 0.25 );  # At most 4 requests per second
  }
  close( CSV );

  my $nheader = 0;
  my $nissue = 0;
  my $nfail = 0;
  my $ncheck = 0;
  for ( my $i = 0;  $i < $n;  $i++ ) {
    open( LST, "$f[$i]" ) || die "Unable to read file: $f[$i]\n";
    my $nl = 0;
    while ( $_ = <LST> ) {
      chop;
#      if ( $nl == 0 && $_ eq "cid" ) { next; }  # potential header row
      if ( $_ eq "" || $_ =~ /\D/ || $_ <= 0 || $_ > 200000000 ) {
        if ( $_ ne "cid" ) {
          print STDERR "File: $f[$i]  Line: $nl  :: Illegal entry: \"$_\"\n";
          $nissue++;
        }

        next;
      }

      $nl++;
    }
    close( LST );

    if ( $nl == 0 ) {
      print STDERR ":: WARNING :: CID count is zero .. $nl in file != $n[$i] expected :: List: $d[$i]\n";
      $nfail++;
    } elsif ( $n[$i] != $nl ) {
      print STDERR ":: WARNING :: CID count consistency check failed .. $nl in file != $n[$i] expected :: List: $d[$i]\n";
      $ncheck++;
    }

    print STDERR "Read $nl lines from file: $f[$i]\n    list: $d[$i]\n";
  }

  print STDERR "\nDownloaded $n lists from $nfl lines with $nissue illegal entries, $ncheck failed CID count consistency checks, and $nfail failures (nCID == 0) via downloaded file: $ifile\n\n";
}

print STDERR "\n\n\n:: Building the classification tree ::\n\n";

my %lh = ();
my %hl = ();

$nl = 0;
open( LST, "$ifile" ) || die "Unable to read: $ifile\n";
#$_ = <LST>;  # Header line
#  print STDERR ":: WARNING :: Skipped header line in file \"$ifile\": $_";
open( WRT, "> treenodedescription.tsv" ) || die "Unable to write: tree_node_description.tsv\n";
while ( $_ = <LST> ) {
  chop;
  my @tmp = split( /	/, $_, 10 );
  if ( $nl == 0 && defined( $tmp[0] ) && $tmp[0] eq "Entry" ) { next; }  # Skip header line
  $nl++;

  if ( $tmp[4] eq "" || $tmp[4] == $tmp[0] ) {
    $tmp[4] = 0;  # Attach to root node
  }

  my $end = chop( $tmp[2] );
  if ( $end eq "\"" ) {  # String is quoted
    substr( $tmp[2], 0, 1 ) = "";  # Remove first quote
  } else {
    $tmp[2] .= $end;  # String is not quoted
  }

  print WRT "$tmp[1]\t$tmp[2]\t$tmp[9]\n";
  $lh{ $tmp[0] } = $tmp[4];
  $hl{ $tmp[0] } = $tmp[1];
}
close( WRT );
close( LST );


# Generate the tree of existing lists
$nl = 0;
my %tree = ();
my %dupcid = ();
open( UMF, "| gzip > treeexistinglists.tsv.gz" ) || die "Unable to write: treeexistinglists.tsv\n";
open( LST, "$ifile" ) || die "Unable to read: $ifile\n";
#$_ = <LST>;  # Header line
#  print STDERR ":: WARNING :: Skipped header line in file \"$ifile\": $_";
my $nfile = 0;
while ( $_ = <LST> ) {
  chop;
  my @tmp = split( /	/, $_, 10 );
  if ( $nl == 0 && defined( $tmp[0] ) && $tmp[0] eq "Entry" ) { next; }  # Skip header line
  $nl++;

  if ( $tmp[4] eq "" || $tmp[4] == $tmp[0] ) {
    $tmp[4] = 0;  # Attach to root node
  }

  if ( ! ( defined( $tmp[7] ) && $tmp[7] ne "" ) ) { next; }

  my $h = $tmp[4];
  my @h = ();
  push( @h, $tmp[1] );
  while ( $h != 0 ) {
    my $ph = $h;
    $h = $lh{ $ph };
    unshift( @h, $hl{ $ph } );
  }
  my $l = join( "\t", @h );

  my @tmp2 = split( /\//, $tmp[7] );
  my $file = "";
  if ( $tmp2[2] eq "pubchem.ncbi.nlm.nih.gov" ) {
    $file = "./treelists/tree_" . $tmp2[6] . "_" . $tmp2[7] . "_" . $tmp2[8] . ".txt";
  } else {
    $file = "./treelists/tree_" . pop( @tmp2 );
  }

  my $nl = 0;
  my %lcid = ();
  open( FLE, "$file" ) || die "Unable to read: $file\n";
#  $_ = <FLE>;  # Header row
#  print STDERR ":: WARNING :: Skipped header line in file \"$file\": $_";
  while ( $_ = <FLE> ) {
    chop;
    $nl++;

    if ( $_ eq "" || $_ =~ /\D/ || $_ <= 0 || $_ > 200000000 ) {
      if ( $_ ne "cid" ) { print STDERR "File: $file  Line: $nl  :: Illegal entry: \"$_\"\n"; }
      next;
    }

    if ( exists( $lcid{ $_ } ) ) {
      if ( exists( $dupcid{ $file } ) ) {
        if ( $dupcid{ $file } < 10 ) {
          print STDERR "  :: WARNING :: Duplicate CID $_ in file: $file\n";
        } elsif ( $dupcid{ $file } == 10 ) {
          print STDERR "  :: WARNING :: Suspended reporting of CID duplicates from file: $file\n";
        }
        $dupcid{ $file }++;
      } else {
        $dupcid{ $file } = 1;
        print STDERR "  :: WARNING :: Duplicate CID $_ in file: $file\n";
      }
    } else {
      $lcid{ $_ } = undef;
      print UMF "$l\t$_\n";
      $tree{ $l }{ $_ } = undef;
    }
  }
  close( FLE );

  my $ncid = keys( %lcid );
  print "Read $ncid CIDs from file: $file\n";
  $nfile++;
}
close( LST );
close( UMF );
foreach my $f ( sort( keys( %dupcid ) ) ) {
  print ":: WARNING :: Detected $dupcid{ $f } duplicate CIDs in file: $f\n";
}
print "Read $nfile files from PFAS lists file: $ifile\n\n\n";


