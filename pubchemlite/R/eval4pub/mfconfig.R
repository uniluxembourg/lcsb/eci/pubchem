## This file contains definitions describing running MetFrag from the
## command line. The mfconfig class collects all the pieces of
## information necessary to assemble a call to MetFrag.

mfconfig <- setClass(Class = "mfconfig",
                     representation = list(
                         ## ID (For example, the cmpd_info row).
                         i = "integer",
                         ## Path to the MetFrag configuration file.
                         fn_conf = "character",
                         ## Path to the MetFrag log file.
                         fn_log = "character",
                         ## Path to the results.
                         fn_res = "character",
                         ## Path to the MetFrag binary.
                         fn_metfrag_jar = "character",
                         ## Path to java binary.
                         fn_java_bin = "character",
                         ## File where the spectra are saved.
                         mem = "character"),
                     prototype = list(
                         ## ID (For example, the cmpd_info row).
                         i = NA_integer_,
                         ## Path to the MetFrag configuration file.
                         fn_conf = NA_character_,
                         ## Path to the MetFrag results file.
                         fn_log = NA_character_,
                         ## Path to the results.
                         fn_res = NA_character_,
                         ## Path to the MetFrag binary.
                         fn_metfrag_jar = NA_character_,
                         ## Path to java binary.
                         fn_java_bin = NA_character_,
                         ## Amount of memory on the heap that java may take.
                         mem = NA_character_))



## Generates spectral and result filenames for all rows in cmpd_info.
gen_mf_fnames <- function(econf,cmpd_info,i_corr=0) {
    dir_spec <- dir_mfspec(econf)
    dir_res <- dir_mfres(econf)
    fn_msms <- sapply(1:length(cmpd_info$PCIDs),function (x) file.path(dir_spec,paste0("spec_",x,".txt")))
    bfn_res <- sapply(1:length(cmpd_info$PCIDs),
                      function(i) paste0(econf@run_name,"_",as.character(i+i_corr)))
    fn_res <- sapply(bfn_res,
                     function (x) file.path(dir_res,paste0(x,".xls")))


    
    list(fn_res=fn_res,
         fn_msms=fn_msms,
         bfn_res=bfn_res)
}

## Generates MetFrag config and peak files for all cmpd_info.
gen_mf_fn_conf <- function(econf,cmpd_info,fn_mfrag,gprops) {
    dir_res <- dir_topres(econf)
    sapply(1:length(cmpd_info$PCIDs),
           function(i) {
               ## check if the MS/MS info is present and should be used
               if (gprops@useMSMS) {
                   msms_file_exists <- cmpd_info$msms_avail[i]
               } else {
                   msms_file_exists <- FALSE
               }
               
               ## come up with the cases 
               if (!msms_file_exists) {
                   ## write silly peaks to MetFrag spec file.
                   msms_peaks <- data.frame(a=9999.9999,b=100)
               } else {
                   ## read in MS/MS
                   msms_file_contents <- cmpd_info$msms_peaks[i]
                   ## read in the peaks
                   msms_peaks <- strsplit(msms_file_contents,";")[[1]]
                   msms_peaks <- sub(":"," ", msms_peaks)
               }
               write.table(msms_peaks,
                           fn_mfrag$fn_msms[[i]],
                           quote=F,
                           row.names=F,
                           col.names = F)

               
               ## do pos/neg detection & find ExactMass
               if (cmpd_info$isPos[i]) {
                   adduct <- "[M+H]+"
                   isPos <- T
               } else {
                   adduct <- "[M-H]-"
                   isPos <- F
               }
               
               lprops <- mfchemprops(mass=cmpd_info$ExactMass[[i]],
                                     neutralPrecursorMass = T,
                                     adduct_type = adduct,
                                     MolForm = cmpd_info$MolecularFormula[[i]])

               ## logme(lprops)

               fn_cfg <- if (gprops@useExactMass) {
                             MetFragConfig(mass = lprops@mass,
                                           adduct_type = lprops@adduct_type,
                                           neutralPrecursorMass=lprops@neutralPrecursorMass, 
                                           results_filename = fn_mfrag$bfn_res[[i]],
                                           ppm=gprops@ppm,
                                           mzabs=gprops@mzabs,
                                           frag_ppm=gprops@frag_ppm, 
                                           peaklist_path = fn_mfrag$fn_msms[[i]],
                                           base_dir = dir_res, 
                                           DB = "LocalCSV",
                                           localDB_path=econf@fn_metfrag_db,
                                           useMonaIndiv = gprops@useMonaIndiv,
                                           useMoNAMetFusion = gprops@useMoNAMetFusion, 
                                           IsPosMode = isPos,
                                           filter_by_InChIKey = gprops@filter_by_InChIKey,
                                           UDS_Category = gprops@UDS_Category,
                                           UDS_Weights = gprops@UDS_Weights)
                         } else {
                             MetFragConfig.formula(mol_form = lprops@MolForm,
                                                   adduct_type = lprops@adduct_type,
                                                   neutralPrecursorMass=lprops@neutralPrecursorMass, 
                                                   results_filename = fn_mfrag$bfn_res[[i]],
                                                   ppm=gprops@ppm,
                                                   mzabs=gprops@mzabs,
                                                   frag_ppm=gprops@frag_ppm, 
                                                   peaklist_path = fn_mfrag$fn_msms[[i]],
                                                   base_dir = dir_res, 
                                                   DB = "LocalCSV",
                                                   localDB_path=econf@fn_metfrag_db,
                                                   useMonaIndiv = gprops@useMonaIndiv,
                                                   useMoNAMetFusion = gprops@useMoNAMetFusion, 
                                                   IsPosMode = isPos,
                                                   filter_by_InChIKey = gprops@filter_by_InChIKey,
                                                   UDS_Category = gprops@UDS_Category,
                                                   UDS_Weights = gprops@UDS_Weights)
                         }
               

               

               
               
               
           })
}

## This constructor is used only internally.
mfconfig_intrn <- function(i,fn_conf,
                         fn_res,
                         fn_metfrag_jar,
                         mem = "",
                         fn_java_bin = "java") {

    not_exist <- which(!file.exists(fn_conf))
    if (length(not_exist) > 0) {
        for (id in not_exist) logmsg("Config file: ", fn_conf[[id]], "does not exist.")
        stop("Config files must exist. Aborting.")
    }
    if (!file.exists(fn_metfrag_jar)) stop("mfconfig: MetFrag runtime ",
                                           fn_metfrag_jar,
                                           " does not exist. Abort")
    fn_log <- gsub("config","log",fn_conf)
    new("mfconfig",i=i,
        fn_conf = fn_conf,
        fn_res = fn_res,
        fn_log = fn_log,
        fn_metfrag_jar = fn_metfrag_jar,
        fn_java_bin = fn_java_bin,
        mem = mem)
}




## Create all bits of information that MetFrag needs before the
## run. User-exposed constructor.
mfconfig <- function(econf,cmpd_info,gprops) {
    ## Generate file names.
    fn_mfrag <- gen_mf_fnames(econf=econf,
                              cmpd_info=cmpd_info,
                              i_corr = gprops@i_corr)

    ## Write config and spectra files.
    fn_cfg <- gen_mf_fn_conf(econf=econf,
                             cmpd_info = cmpd_info,
                             fn_mfrag = fn_mfrag,
                             gprops = gprops)

    mfconfig_intrn(i=1:nrow(cmpd_info),
                   fn_res = fn_mfrag$fn_res,
                   fn_conf = fn_cfg,
                   fn_metfrag_jar = econf@fn_metfrag_jar,
                   fn_java_bin = econf@fn_java_bin,
                   mem = "4g")

}

metfrag_run <- function(fn_jar, fn_conf, fn_log, mem = "", java_bin = "java") {
    ## Check if file exists.

    ## Assemble arguments.
    args <- c('-jar',fn_jar,fn_conf)
    ## If total heap memory given (in quantities like '4m', or '2g')
    ## then make this argument.
    if (nchar(mem)>0) args <- c(paste0('-Xmx', mem),args)
    ## Start new java process.
    p <- process$new(java_bin,args=args,stdout=fn_log,stderr='2>&1')
    p$wait()
    p$get_exit_status()
    
}

## Takes a list of mfconfig objects and runs MetFrag on them in
## possibly simultaneous futures (depending on the plan). But, use the
## `run' method defined below.
mfconfig_run <- function(x) {
    ntasks <- length(x)
    logmsg("Total tasks:",ntasks)
    procs <- list()
    fn_jar <- x@fn_metfrag_jar
    mem <- x@mem
    java_bin <- x@fn_java_bin
    for (n in 1:ntasks) {
        fn_conf <- x@fn_conf[[n]]
        fn_log <- x@fn_log[[n]]
        procs <- c(future::future({
            
            st <- metfrag_run(fn_jar = fn_jar,
                              fn_conf = fn_conf,
                              fn_log = fn_log,
                              mem = mem,
                              java_bin = java_bin)
            
            
        }), procs)

        logmsg("Completed MetFrag tasks: ", length(which(sapply(procs,future::resolved))), "/",ntasks)
    }

    while (1==1) {
        compl <- length(which(sapply(procs,future::resolved)))
        logmsg("Completed MetFrag tasks: ", compl, "/",ntasks)
        if (ntasks == compl) break
        Sys.sleep(5)
    }

}

## Process metfrag runs and fill cmpd_info based on the results.
procit <- function(x,econf,cmpd_info,gprops) {
    for (i in x@i) {
        mf_res <- read_excel(x@fn_res[[i]])
        if (length(mf_res) > 0) {
            cmpd_info[i,] <- user_fill_cmpd_info(mf_res,
                                                 cmpd_info[i,],
                                                 pre2020 = gprops@pre2020,
                                                 IKmatch = T,
                                                 IK = as.character(cmpd_info[i,"InChIKey"]),
                                                 extra_terms = gprops@extra_terms)
        } else cmpd_info[i,"num_poss_IDs"] <- 0
    }
    
    
    
    
    ## create summary csv
    write.csv(cmpd_info,summary_fn(econf),row.names = F)
    cmpd_info
}



setMethod("length",signature = "mfconfig", definition = function(x) {
    length(x@fn_res)
})

setMethod("run", signature = "mfconfig", mfconfig_run)


