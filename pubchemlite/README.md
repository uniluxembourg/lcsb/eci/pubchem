## PubChemLite Build System

The PubChemLite build and deposition system has been migrated to a separate repository. 
* PubChemLite Build System: https://gitlab.com/uniluxembourg/lcsb/eci/pclbuild
* PubChemLite Input files: https://gitlab.com/uniluxembourg/lcsb/eci/pubchemlite-input

Code/files remaining in this repository are related to the PubChemLite publication 
(DOI: [10.1186/s13321-021-00489-0](https://doi.org/10.1186/s13321-021-00489-0))
and to other PubChem-ECI efforts.
