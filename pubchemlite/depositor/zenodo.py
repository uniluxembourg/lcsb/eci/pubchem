import json
import sys
import os
import os.path
import requests
import glob
import mistletoe
import re
import ruamel.yaml as yaml
from pathlib import Path
import sys
from mistletoe.ast_renderer import ASTRenderer
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import hashlib
import logging
import pickle

EXTS= ['.csv','.csv.gz','.csv.bz2','.csv.xz','.csv.zip']

def init_req_session():
    """Do not annoy Zenodo excessively."""
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return(session)
    
def get_datestamp(pref,fn):
    rex=re.compile(pref+r'(\d\d\d\d\d\d\d\d+)\..*')
    res=rex.sub(r'\1',fn)
    if res==fn:
        logging.error('Unable to find the datestamp with prefix '+pref+' in the file '+fn+' .')
        sys.exit('Fatal error. Exit.')
    return(res)



def upd_semver_minor(meta,old_version=None):
    new_major=meta['version']
    new_patch='0'
    if old_version:
        things=old_version.split('.')
        if len(things)>=3:      # This should always be the case...
            old_major=things[0]
            old_minor=things[1]
            old_patch=things[2]
        elif len(things)==2:    # ... but if it isn't, X.Y -> X.Y.0
            old_major=things[0]
            old_minor=things[1]
            old_patch='0'
        elif len(things)==1:    # ... X -> X.0.0
            old_major=things[0]
            old_minor='0'
            old_patch='0'
            
        if int(old_major)==int(new_major):
            new_minor=str(int(old_minor)+1)
        elif int(new_major)>int(old_major):
            new_minor='0'
        else:
            sys.exit('Fatal error. Major version in publish.md less than the Zenodo major version.')
    else:
        new_minor='0'
    meta['version']=new_major+'.'+new_minor+'.'+new_patch
    return(meta)

def hash_bytestr_iter(bytesiter, hasher, ashexstr=True):
    # Source: SO, Q: 3431825 (generating-an-md5-checksum-of-a-file)
    for block in bytesiter:
        hasher.update(block)
    return hasher.hexdigest() if ashexstr else hasher.digest()

def file_as_blockiter(afile, blocksize=65536):
    # Source: SO, Q: 3431825 (generating-an-md5-checksum-of-a-file)
    with afile:
        block = afile.read(blocksize)
        while len(block) > 0:
            yield block
            block = afile.read(blocksize)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)



def depo_create(session,depoconf,meta):
    """Creates a brand new deposition."""
    access_token = depoconf['access_token']
    depo_url = depoconf['url_zenodo']
    headers = {"Content-Type": "application/json"}
    params = {'access_token': access_token}
    logging.info("Uploading to %s" % depo_url)
    oldver=meta['version']
    meta.pop('version',None)
    data = {'metadata':meta}
    logging.info('Creating deposition...')
    r = session.post(url = depo_url,
                     params=params,
                     data=json.dumps(data),
                     headers=headers)

    if r.status_code != 201:
        eprint(r.json())
        sys.exit('Fatal error. Exit.')

    jdepo=r.json()
    depoid=jdepo['id']
    fn_depoid=depoconf['local']+'/'+'depoid'
    with open(fn_depoid,'wb') as f:
        pickle.dump(depoid,f)
    logging.info('ID saved for future reference to depo %s',depoid) 
        
    logging.info('Creating deposition...done')
    meta['version']=oldver      # Perhaps even not needed. Are dicts
                                # passed by reference?
    return(r.json())


def depo_publish(session,depoconf,meta,jdepo):
    """Finalises changes by publishing a deposition."""
    url_zenodo=depoconf['url_zenodo']
    access_token=depoconf['access_token']
    depoid=jdepo['id']
    r = session.post('%s/%s/actions/publish' % (url_zenodo,depoid),
                     params={'access_token': access_token})
    if r.status_code != 202:
        eprint(r.json())
        sys.exit('Fatal error when publishing. Exit.')
    fn_depoid=depoconf['local']+'/'+'depoid'
    with open(fn_depoid,'wb') as f:
        pickle.dump(depoid,f)

    return(r.json)

def depo_edit(session,depoconf,meta,jdepo):
    """Unlocks deposition for editing."""
    url_zenodo=depoconf['url_zenodo']
    access_token=depoconf['access_token']
    depoid=jdepo['id']
    if jdepo['state']!='unsubmitted':
        logging.info('Unlocking deposition for editing.')
        r = session.post('%s/%s/actions/edit' % (url_zenodo,depoid),
                         params={'access_token': access_token})
        if r.status_code != 201:
            eprint(r.json())
            sys.exit('Fatal error when unlocking for editing. Exit.')
        jdepo=r.json()
    return(jdepo)
    
def depo_update_meta(session,depoconf,meta,jdepo):
    """Modifies deposition metadata."""
    url_zenodo=depoconf['url_zenodo']
    access_token=depoconf['access_token']
    depoid=jdepo['id']
    url = "%s/%s?access_token=%s" % (url_zenodo,depoid,access_token)
    headers = {"Content-Type": "application/json"}
    r = session.put(url, data=json.dumps({'metadata':meta}), headers=headers)
    logging.info('Uploaded new metadata (if any).')
    if r.status_code != 200:
        logging.error(r.json())
        sys.exit('Fatal error when updating metadata. Exit.')
    return(r.json())

def depo_files_query(session,depoconf,jdepo):
    """Query which files have changed."""
    access_token=depoconf['access_token']
    files_url=jdepo['links']['files']
    dir_pub=depoconf['local']+"/publish"
    fn_prefix=depoconf['monitor']
    loc_files=os.listdir(dir_pub)
    re_prefix={fnp:re.compile(fnp+"[0-9]+") for fnp in fn_prefix}

    # prefix <--> local file
    loc_files_match={fnp:sorted(filter(re_prefix[fnp].match,loc_files)) for fnp in fn_prefix}
    loc_files_match={fnp:loc_files_match[fnp][len(loc_files_match[fnp])-1] for fnp in fn_prefix if len(loc_files_match[fnp])>0}

    # Filter disallowed extensions.
    loc_files_match={fnp:loc_files_match[fnp] for fnp in loc_files_match.keys() if any([loc_files_match[fnp].endswith(ext) for ext in EXTS])}

    # prefix <--> datestamp
    loc_files_stamps={fnp:get_datestamp(fnp,loc_files_match[fnp]) for fnp in loc_files_match.keys()}

    # prefix <--> checksum
    loc_files_csums={fnp:hash_bytestr_iter(file_as_blockiter(open(dir_pub+'/'+loc_files_match[fnp], 'rb')), \
                                           hashlib.md5())\
                     for fnp in loc_files_match.keys()}
                     

    
    zenodo_files= session.get(files_url,params={'access_token':access_token}).json()

    # filename <--> zenodo file record
    zenodo_files={o['filename']:o for o in zenodo_files}


    # prefix <--> zenodo filename
    fns_zenodo=zenodo_files.keys()
    fns_zenodo_match={fnp:sorted(filter(re_prefix[fnp].match,fns_zenodo)) for fnp in fn_prefix}
    fns_zenodo_match={fnp:fns_zenodo_match[fnp][len(fns_zenodo_match[fnp])-1] for fnp in fn_prefix if len(fns_zenodo_match[fnp])>0}

    # prefix <--> datestamp
    fns_zenodo_stamps={fnp:get_datestamp(fnp,fns_zenodo_match[fnp]) for fnp in fns_zenodo_match.keys()}
    # fns_nonext=list(set(files.keys())-set(zenodo_files.keys()))
    # fns_samename=list(set(files.keys())-set(fns_nonext))
    files={}
    for fnp in loc_files_match.keys():
        fn=loc_files_match[fnp]
        if fnp in fns_zenodo_match:
            zfn=fns_zenodo_match[fnp]
            lstamp=loc_files_stamps[fnp]
            zstamp=fns_zenodo_stamps[fnp]
            lcsum=loc_files_csums[fnp]
            zcsum=zenodo_files[fns_zenodo_match[fnp]]['checksum']
            zid=zenodo_files[fns_zenodo_match[fnp]]['id']
            if lstamp>zstamp or (lstamp==zstamp and lcsum!=zcsum):
                files[fnp]={'new':fn,'old':zfn,'id':zid}
        else:
            files[fnp]={'new':fn,'old':None,'id':None}


        
    # for fn in fns_samename:
    #     loc_chk=files[fn]['checksum']
    #     rem_chk=zenodo_files[fn]['checksum']
    #     if loc_chk!=rem_chk:
    #         fns_refresh[fn]=zenodo_files[fn]['id']

    return(files)
            
def depo_semver_stamp(meta,jdepo,what='update'):
    """Query which files have changed."""

    if what == 'create' or (not 'version' in jdepo['metadata'].keys()):
        meta=upd_semver_minor(meta)
        logging.info('Stamping upload with version: %s .',meta['version'])
    elif what == 'update':
        meta=upd_semver_minor(meta,old_version=jdepo['metadata']['version'])
        logging.info('Stamping upload with version: %s',meta['version'])
    else:                       # Keep old.
        meta['version']=jdepo['metadata']['version']
        logging.info('Keeping old version: %s',meta['version'])


    return(meta)
        

    

def depo_files_upload(session,depoconf,meta,jdepo,files):
    """Query which files have changed."""
    access_token=depoconf['access_token']
    dir_pub=depoconf['local']+"/publish"
    url_zenodo=depoconf['url_zenodo']
    depoid=jdepo['id']
    
    if len(files)==0:
        return(jdepo,meta)
    
    if jdepo['submitted']:

        r = session.post('%s/%s/actions/newversion' % (url_zenodo,depoid),
                              params={'access_token': access_token})
        if r.status_code!=201:
            eprint(r.json())
            logging.info('Fatal error when creating new Zenodo version.')
        with open('new_version.json','w') as f:
            print(json.dumps(r.json(),indent=4),file=f)

        r = session.get(r.json()['links']['latest_draft'],
                        params={'access_token': access_token},
                        headers = {"Content-Type": "application/json"})
        if r.status_code!=200:
            eprint(r.json())
            logging.info('Fatal error when creating new Zenodo version.')
        with open('draft.json','w') as f:
            print(json.dumps(r.json(),indent=4),file=f)
        jdepo=r.json()
        with open('new_version.json','w') as f:
            print(json.dumps(jdepo,indent=4),file=f)


    files_url=jdepo['links']['bucket']
    exist_files_url=jdepo['links']['files']
    url_upl=files_url+'?access_token='+access_token
    for fnp in files.keys():
        fn_new=files[fnp]['new']
        fn_old=files[fnp]['old']
        zid=files[fnp]['id']

        if fn_old:
            session.delete(exist_files_url+"/"+zid,
                           params={'access_token': access_token})
            logging.info('Deleted old version of %s',fn_old)

        with open(dir_pub+'/'+fn_new,'rb') as f:
            r=session.put(url=files_url + "/" + fn_new,
                          data=f,
                          params={'access_token': access_token})
            if r.status_code!=200:
                logging.error('Unable to upload file %s',fn_new)
                logging.error(r.json())
                sys.exit('Fatal error. Exit.')
            else:
                logging.info('Uploaded file %s',fn_new)
        

    return(jdepo,meta)
    



def depo_query(session,depoconf,meta):
    title=meta['title']
    depo_url=depoconf['url_zenodo']
    access_token=depoconf['access_token']
    fn_depoid=depoconf['local']+'/'+'depoid'

    depo_id=None
    jdepo=None
    if (os.path.exists(fn_depoid)):
        with open(fn_depoid,'rb') as f:
            depo_id=str(pickle.load(f))
                 
        r = session.get(depo_url+"/"+depo_id+"?access_token="+access_token)
        if r.status_code != 200:
                 logging.error(r.json())
                 logging.error("Unable to retrieve deposition with id: %s",depo_id)
                 sys.exit('Fatal error while retrieving deposition. Exit.')

        logging.info("Found deposition with id: %s",depo_id)
        jdepo=r.json()
    elif depoconf['title_query_on_create']=='yes':
        logging.info("Not found any previously saved deposition, trying to search for it.")
        r = session.get(depo_url,
                        params={'q': title,
                                'sort':'mostrecent',
                                'size':1,
                                'access_token': access_token})
        if r.status_code!=200:
            logging.error('Error while searching')
            if (r.status_code==500):
                logging.error('Got code 500, it is either something on their side, or, possibly, our title is malformed.')
            logging.error(r.json())
            sys.exit('Fatal error while searching. Exit.')
            
        if (len(r.json())==0 or r.json()[0]['title']!=title):
            logging.info("Deposition with the title `%s', has not been found.",title) 
            return(None)
        jdepo=r.json()[0]

                 

    fh=open('depoquery.json','w')
    print(json.dumps(jdepo,indent=4),file=fh)
    fh.close()
        
    return(jdepo)

def parse_text2html(item):
    stuff=""
    if item.get('type')=='Paragraph':
        for txt in item.get('children'):
            if txt.get('type')=='RawText':
                stuff+=txt['content']
            elif txt.get('type')=='LineBreak':
                stuff+="\n"
            elif txt.get('type')=='Strong':
                stuff+='<b>'+txt.get('children')[0].get('content')+'</b>'
            elif txt.get('type')=='Emphasis':
                stuff+='<em>'+txt.get('children')[0].get('content')+'</em>'
            elif txt.get('type')=='Link':
                stuff+='<a href="'+txt.get('target')+'">'+\
                    txt.get('children')[0].get('content')+'</a>'
            else:
                stuff+="____UNRECOGNISEDTOKEN____"
        stuff= "<p>" + stuff + "</p>\n" # Divide paragraphs.
    return(stuff)



def parse_publish(fn_publish):

    # Remove MarkDown comments, otherwise they'll confuse the parser.
    text=re.sub("(<!--.*?-->)", "", Path(fn_publish).read_text(encoding="utf-8"), flags=re.DOTALL)

    # Return the md syntax tree in content.
    with ASTRenderer() as renderer:
        content=json.loads(renderer.render(mistletoe.Document(text)))['children']


    # Pick up things we need.
    meta={'description':"",
          'notes':"",
          'creators':[],
          'contributors':[],
          'references':[],
          'communities':[],
          'keywords':[],
          'related_identifiers':[],
          'upload_type':'dataset',
          'license':'cc-by',
          'access_right':'open',
          'access_conditions':''}
    nextisdesc=False
    topic=None
    listtopic=None
    creator=None
    contributor=None
    for item in content:
        if item.get('level')==1:
            meta['title']=item['children'][0]['content']

        elif item.get('type')=='Heading' and \
             item.get('level')==2:
            topic=item.get('children')[0]['content']
            topic=topic.lower()

        elif topic=='description':
            meta['description']+=parse_text2html(item)

        elif topic=='notes':
            meta['notes']+=parse_text2html(item)

        elif topic=='creators':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                creator={'name':name}
            elif item.get('type')=='List' and creator:
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                creator[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                creator[key]=val
                meta['creators'].append(creator)
                creator=None
                
        elif topic=='contributors':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                contributor={'name':name}
            elif item.get('type')=='List' and contributor:
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                contributor[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                contributor[key]=val
                meta['contributors'].append(contributor)
                contributor=None

        elif topic=='references':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['references'].append(ref)

        elif topic=='communities':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['communities'].append({'identifier':ref.strip()})

        elif topic=='keywords':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['keywords'].append(ref)

        
        elif topic=='related identifiers':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                relident={}
            elif item.get('type')=='List':
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                relident[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                relident[key]=val
                meta['related_identifiers'].append(relident)
                relident=None

        elif topic=='major version':
            if item.get('type')=='Paragraph':
                meta['version']= item.get('children')[0]['content']

        elif topic=='license':
            if item.get('type')=='Paragraph':
                meta['license']= item.get('children')[0]['content']

        elif topic=='access right':
            if item.get('type')=='Paragraph':
                meta['access_right']= item.get('children')[0]['content']
                
        elif topic=='access conditions':
            if item.get('type')=='Paragraph':
                meta['access_conditions']=parse_text2html(item)


        
                    



    if (len(meta['communities'])==0):
        meta.pop('communities',None)

    if (len(meta['contributors'])==0):
        meta.pop('contributors',None)

    if meta['access_right']!='restricted':
        meta.pop('access_conditions',None)
    
    return(meta,content)

def depo_discard_edit(session,depoconf,meta,jdepo):
    url_zenodo=depoconf['url_zenodo']
    access_token=depoconf['access_token']
    depoid=jdepo['id']
    if jdepo['state']=='error':
        logging.error('Deposition is in erroneous state.')
        sys.exit('Fatal error. Exit.')
        
    if jdepo['state']!='done':
        logging.info('Cleaning up deposition.')
        r = session.post('%s/%s/actions/discard' % (url_zenodo,depoid),
                         params={'access_token': access_token})
        if r.status_code != 201:
            logging.error(r.json())
            sys.exit('Fatal error when discarding aborted changes. Exit.')

        logging.info('Requerying deposition after discarding.')
        jdepo=depo_files_query(session,depoconf,meta)
    return(jdepo)
