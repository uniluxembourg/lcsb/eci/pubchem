# Current

## Data source

The NORMAN network enhances the exchange of information on emerging environmental substances, and encourages the validation and harmonisation of common measurement methods and monitoring tools so that the requirements of risk assessors and risk managers can be better met. The NORMAN Suspect List Exchange (NORMAN-SLE) is a central access point to find suspect lists relevant for various environmental monitoring questions, described in DOI:10.1186/s12302-022-00680-6

## Classification Description

The NORMAN Suspect List Exchange (NORMAN-SLE) is a central access point to find suspect lists relevant for various environmental monitoring questions, described in Mohammed Taha et al. (2022) DOI:[10.1186/s12302-022-00680-6](https://doi.org/10.1186/s12302-022-00680-6). 
[Update stamp]
This Exchange documents all individual collections that (will) form a part of NORMAN [SusDat](https://www.norman-network.com/nds/susdat/), the merged NORMAN Substance Database. Each list/collection has a DOI in [Zenodo](https://zenodo.org/communities/norman-sle)

## Classification Top Tool Tip
The NORMAN Suspect List Exchange (NORMAN-SLE) is a central access point point to find suspect lists relevant for various environmental monitoring questions, described in Mohammed Taha et al. (2022) DOI:[10.1186/s12302-022-00680-6](https://doi.org/10.1186/s12302-022-00680-6)


# Previous (pre-29/03/2023)

## Data source
The NORMAN network enhances the exchange of information on emerging environmental substances, and encourages the validation and harmonisation of common measurement methods and monitoring tools so that the requirements of risk assessors and risk managers can be better met. It specifically seeks both to promote and to benefit from the synergies between research teams from different countries in the field of emerging substances.

## Classification Description
The NORMAN Suspect List Exchange (NORMAN-SLE) is a central access point for NORMAN members (and others) to find suspect lists relevant for their environmental monitoring questions.
[Update stamp]
This Exchange documents all individual collections that (will) form a part of NORMAN SusDat, the merged NORMAN Substance Database (DOI:10.5281/zenodo.2664077). Each list/collection has a DOI in [Zenodo](https://zenodo.org/communities/norman-sle)

## Classification Top Tool Tip
The NORMAN Suspect List Exchange (NORMAN-SLE) is a central access point for NORMAN members (and others) to find suspect lists relevant for their environmental monitoring questions.
[Update stamp]
