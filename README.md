# PubChem

A project for interactions with PubChem

## PubChem/NORMAN-SLE Integration
* [Transformations Documentation](annotations/tps/README.md)
* [Deposition](deposition) (internal use only)
* [Annotation Efforts](annotations) (internal use only)

## PubChem/MassBankEU Integration
* [Deposition and Annotation Workflows](massbank_eu/README.md)

## PubChemLite
* ***Download the latest PubChemLite at DOI:[10.5281/zenodo.5995885](https://doi.org/10.5281/zenodo.5995885)*** 
(updating monthly)

Major PubChemLite Versions:

 Version  | Description    |DOI/URL
----------|----------------|--------
 ***Latest*** | ***PCL-Exposomics (latest)*** | ***DOI:[10.5281/zenodo.5995885](https://doi.org/10.5281/zenodo.5995885)***
01/01/2021| PCL-Exposomics | DOI:[10.5281/zenodo.4432124](https://doi.org/10.5281/zenodo.4432124)
01/01/2021| PCL-Exposomics + CCS | DOI:[10.5281/zenodo.4456208](https://doi.org/10.5281/zenodo.4456208)
31/10/2020| PCL-Exposomics | DOI:[10.5281/zenodo.4183801](https://doi.org/10.5281/zenodo.4183801)
14/01/2020| PCL-tier0&tier1 | DOI:[10.5281/zenodo.3611238](https://doi.org/10.5281/zenodo.3611238)

Currently PubChemLite is building weekly and committed monthly to the DOI redirecting to the 
"[***latest***](https://doi.org/10.5281/zenodo.5995885)" version. 
Archives of other PubChemLite versions are visible within these Zenodo records. 

## PubChemLite Build System
The PubChemLite build and deposition system has been migrated to a separate repository.  
* PubChemLite Build System: https://gitlab.lcsb.uni.lu/eci/pclbuild
* PubChemLite Input files: https://gitlab.lcsb.uni.lu/eci/pubchemlite-input

Code/files located in this [pubchemlite](pubchemlite/README.md) folder are related 
to the publication 
DOI:[10.1186/s13321-021-00489-0](https://doi.org/10.1186/s13321-021-00489-0) 
and other efforts.

## License
Content in this repository is shared under these license conditions:
* Code: Artistic-2.0
* Data: CC-BY 4.0 [(Creative Commons Attribution 4.0 International)](http://creativecommons.org/licenses/by/4.0/legalcode)

Copyright (c) 2017-2024 University of Luxembourg
